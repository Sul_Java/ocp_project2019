package set;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetMitKreisen {
	
	static class Kreis implements Comparable<Kreis>{
		private int radius;
		
		public Kreis(int radius) {
			this.radius = radius;
		}
		
		@Override
		public String toString() {
			return "K" + radius;
		}

		@Override
		public int compareTo(Kreis k2) {
			return radius - k2.radius;
		}
		
	}
	
	public static void main(String[] args) {
		
		Set<Kreis> set = new TreeSet<>();
		
		set.add(new Kreis(5));
		set.add(new Kreis(5));
		
		System.out.println(set.size());
		System.out.println(set);
		
	}

}
