package set;

import java.util.HashSet;

class Blume{
	private int size;
	
	public Blume(int size) {
		this.size = size;
	}
	
	@Override
	public String toString() {
		return "B-" + size;
	}
	
	@Override
	public int hashCode() {
		return size;
	}
	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Blume b2 = (Blume) obj;
		return size == b2.size;
	}
}

public class HshSetMitBlumen {
	
	public static void main(String[] args) {
		
		Blume b1 = new Blume(3);
		
		HashSet<Blume> set = new HashSet<Blume>();
		
		set.add(b1);
		set.add(b1);
		
		Blume b2 = new Blume(3);
		
		set.add(b2);
		
		
		
		System.out.println(set.size());
		
	}

}
