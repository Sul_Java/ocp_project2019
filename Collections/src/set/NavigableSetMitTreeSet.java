package set;

import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class NavigableSetMitTreeSet {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> basisSet = new TreeSet<>();
		basisSet.add(33);
		basisSet.add(17);
		basisSet.add(-9);
		
		System.out.println(basisSet);
		
		Integer toElement = 33 ;
		SortedSet<Integer> sortedSet = basisSet.headSet(toElement);
		System.out.println(sortedSet);
		
		boolean inclusive = true;
		NavigableSet<Integer> headSet2 = basisSet.headSet(toElement, inclusive );
		
		System.out.println(headSet2);
		
		SortedSet<Integer> tailSet = basisSet.tailSet(0);
		System.out.println(tailSet);
		
		basisSet.add(-11);
		basisSet.add(5);
		System.out.println(basisSet);
		System.out.println(tailSet);
		
	}

}
