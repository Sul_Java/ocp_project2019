package set;

import java.util.HashSet;

public class HashSetWithInteger {
	
	public static void main(String[] args) {
		
		HashSet<Integer> set = new HashSet<Integer>();
		
		set.add(9);
		set.add(17);
		set.add(9);
		set.add(-11);
		set.add(33);
		set.add(9);
		
		System.out.println(set); // output : [17, 33, 9, -11]
		
		
		
	}

}
