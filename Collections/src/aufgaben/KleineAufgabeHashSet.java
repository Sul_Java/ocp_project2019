package aufgaben;

import java.util.Collection;
import java.util.HashSet;

class Kreis{
	private int radius;
	
	public Kreis(int radius) {
		this.radius = radius;
	}

	@Override
	public int hashCode() {
		return radius;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Kreis)) {
			return false;
		}
		
		Kreis k1 = (Kreis) obj;
		return radius == k1.radius;
	}
	
	
}

public class KleineAufgabeHashSet {

	public static void main(String[] args) {
		
		HashSet<Kreis> set = new HashSet<Kreis>();
		Kreis k1 = new Kreis(5);
		Kreis k2 = new Kreis(5);
		set.add(k1);
		set.add(k2);
		set.add(new Kreis(6));
		System.out.println(set.size());
		
		Collection<Kreis> c = new HashSet<Kreis>();
		c.add(k1);
		c.add(new Kreis(5));
		System.out.println(c.size());

	}

}
