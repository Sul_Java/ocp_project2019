package aufgaben;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class AufgabeSetAbfahrzeiten {
	
	public static void main(String[] args) {
		
		List<String> abfahrtzeiten = getAbfahrtzeiten(6 , 24 , 20);
		
		System.out.println(abfahrtzeiten);
		

		TreeSet<String> treeSet = new TreeSet<String>(abfahrtzeiten);
		
		System.out.println(treeSet.higher("12:03"));
		System.out.println(treeSet.floor("12:03"));
		System.out.println(treeSet.ceiling("17:12"));
		System.out.println(treeSet.higher("17:12"));
		System.out.println(treeSet.subSet("12:00", "13:00"));
		System.out.println(treeSet.subSet("11:52", false, "13:12", true));
		System.out.println(treeSet.first());
		System.out.println(treeSet.last());
		
	}

	private static List<String> getAbfahrtzeiten(int start, int end , int minuten) {
		
		List<String> list = new ArrayList<String>();
		
		for (int i = start; i < end; i++) {
			for (int j = 12; j <= 52; j += 20) {
				String zeit = String.format("%02d:%2d", i , j);
				list.add(zeit);
			}
		}
		return list;
	}
	
//	static List<LocalTime> getAbfahrtzeiten(LocalTime start , LocalTime ende , int minutenTakt){
//		return null;
//	}

}
