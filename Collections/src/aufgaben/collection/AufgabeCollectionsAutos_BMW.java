package aufgaben.collection;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class AufgabeCollectionsAutos_BMW {
	
	public static void main(String[] args) {
		
		System.out.println("**************** A6");
		
		BMW bmw1 = new BMW("Z4", 2000);
		BMW bmw2 = new BMW("Z6", 2015);
		
		ArrayList<BMW> arrayListBMW = new ArrayList<>();
		arrayListBMW.add(bmw1);
		arrayListBMW.add(bmw2);
		print(arrayListBMW);
		
		HashSet<BMW> hashSetBMW = new HashSet<>(arrayListBMW);
		print(hashSetBMW);
	
		Set<BMW> treeSetBMW = new TreeSet<>(arrayListBMW);
		print(treeSetBMW);

		ArrayDeque<BMW> arrayDequeBMW = new ArrayDeque<>(arrayListBMW);
		print(arrayDequeBMW);
		
		System.out.println("***************** A7");
		
		System.out.println(hashSetBMW.contains(bmw1));
//		
		bmw1.setBaujahr(2005);
		print(hashSetBMW);
		System.out.println(hashSetBMW.contains(bmw1));
		
	}
	
	static void print(Collection<BMW> autos) {
		System.out.println(autos.getClass().getSimpleName());
		Iterator<BMW> it = autos.iterator();
		
		for (int i = 0; i < autos.size(); i++) {
			System.out.printf("%d. %s \n", i , it.next());
		}
	}

}
