package aufgaben.collection;

public abstract class Auto <T extends Auto> implements Comparable<T> {

	 int baujahr;
	 String modell;
	
	public Auto(String modell , int baujahr) {
		this.baujahr = baujahr;
		this.modell = modell;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ". Modell: " + modell + " , Baujahr " + baujahr;
	}
	
	public boolean equals(Object obj) {
		if(getClass() != obj.getClass()) {
			return false;
		}
		Auto<T> a2 = (Auto<T>) obj;
		return modell.equals( a2.modell ) && baujahr == a2.baujahr;
	}
	@Override
	public int hashCode() {
		return modell.hashCode() + baujahr * 31;
	}
	
	@Override
	public int compareTo(T a) {
		int erg = modell.compareTo(a.modell);
		if (erg == 0 ) {
			erg = baujahr - a.baujahr;
		}
		return erg;
	}
	
}
