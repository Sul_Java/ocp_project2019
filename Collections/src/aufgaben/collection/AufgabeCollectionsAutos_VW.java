package aufgaben.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class AufgabeCollectionsAutos_VW {
	
	public static void main(String[] args) {
		
		System.out.println("************ A2");
		
		VW vw1 = new VW("Golf", 1990);
		System.out.println(vw1);
		
		BMW bmw1 = new BMW("Z4", 2000);
		System.out.println(bmw1);
		
		// ************ A3
		
		VW vw2 = new VW("Polo", 1998);
		VW vw3 = new VW("Golf", 2016);
		
		System.out.println("************ A4");
		
		List<VW> list = Arrays.asList(
				vw1,
				vw2,
				vw3,
				vw1
			);
		
		LinkedList<VW> linkedListVW = new LinkedList<>();
		linkedListVW.addAll(list);
		print(linkedListVW);
		
		Comparator<VW> cmp = (v1 , v2) -> {
			int erg = v1.modell.compareTo(v2.modell);
			if (erg == 0) {
				erg = v1.baujahr - v2.baujahr;
			}
			return erg;	
		};
		Collections.sort(linkedListVW, cmp);
		System.out.println("Sorted LinkedList : -> " + linkedListVW);
		
		HashSet<VW> hashSetVW = new HashSet<>(linkedListVW);
		print(hashSetVW);
		
		
		
		TreeSet<VW> treeSetVW = new TreeSet<>(linkedListVW);
		print(treeSetVW);
		
		PriorityQueue<VW> priorityQueueVW = new PriorityQueue<>(linkedListVW);
		print(priorityQueueVW);
		
		
		System.out.println("**************** A5");
		printCollections(linkedListVW);
		printCollections(hashSetVW);
		printCollections(treeSetVW);
		printCollections(priorityQueueVW);
		
	}
	
	static void printCollections(Collection<VW> coll) {
		System.out.println(coll.getClass().getSimpleName());
		for (VW vw : coll) {
			System.out.println(vw);
		}
	}

	static void print(Collection<VW> autos) {
		System.out.println(autos.getClass().getSimpleName());
		Iterator<VW> it = autos.iterator();
		
		for (int i = 0; i < autos.size(); i++) {
			System.out.printf("%d. %s \n", i , it.next());
		}
	}
}
