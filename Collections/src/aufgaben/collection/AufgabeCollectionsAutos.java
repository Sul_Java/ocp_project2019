package aufgaben.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AufgabeCollectionsAutos {
	
	public static void main(String[] args) {
	
		VW vw1 = new VW("Golf", 1990);
		VW vw2 = new VW("Audi", 1998);
		VW vw3 = new VW("A6", 2004);
		VW vw4 = new VW("Polo", 2200);
		
		List<VW> listVW = new ArrayList<>();
		listVW.add(vw1);
		listVW.add(vw2);
		listVW.add(vw3);
		listVW.add(vw4);
		
		VW vwSearch = new VW("Polo", 2200);
		int index = Collections.binarySearch(listVW, vwSearch);
		System.out.println(index);
		
		System.out.println("***************** A11");
		
		System.out.println("Unsorted list : " + listVW);
		Collections.sort(listVW);
		System.out.println("Sorted list : " + listVW);
		
		System.out.println("***************** A12");
		
		Comparator<VW> cmp = Comparator.reverseOrder();
		Collections.sort(listVW, cmp );
		System.out.println("reversed order list : " +listVW);
		
		System.out.println("***************** A13");
		index = Collections.binarySearch(listVW, vwSearch);
		System.out.println(index);
		
	}

}
