package aufgaben.list_benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class AufgabeListBenchmark {
	
	static final int N = 10_000;
	static final int M = 100;
	
	public static void main(String[] args) {
		
		
		
		
	}
	
	static void testList(Supplier<List<String>> listSupplier) {
		
		long timeTotale = 0;
		
		for (int i = 0; i < M; i++) {
			List<String> target = new ArrayList<>();
			long startCall = System.currentTimeMillis();
			testAddFirst(N, target);
			long endCall = System.currentTimeMillis();
			
			long timeCall = endCall - startCall;
			
			timeTotale += timeCall;
		}
		
		System.out.println(timeTotale / M);
	}

	static void testAddFirst(int numberOfString , List<String> target) {
		for (int i = 0; i < numberOfString; i++) {
			String s = ""+i;
			target.add(0,s);
		}
	}
	
}
