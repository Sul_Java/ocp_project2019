package aufgaben.set_textStatistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;

public class TextStatistics {

	private String text;
	
	protected TextStatistics(String text) {
		this.text = text;
	}

	public static TextStatistics of(String text) {
		return new TextStatistics(text);
	}

	public Collection<Character> getUniqueChars() {
		
//		Collection<Character> coll = new HashSet<>();
		
		Collection<Character> coll = new TreeSet<>();
		
		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			coll.add(ch);
		}
		
		return coll;
	}

}
