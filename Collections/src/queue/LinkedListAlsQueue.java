package queue;

import java.util.LinkedList;
import java.util.Queue;

public class LinkedListAlsQueue {

	public static void main(String[] args) {
		
		Queue<Integer> intQueue = new LinkedList<>();
		
		intQueue.add(7);
		intQueue.add(3);
		
		
		intQueue.offer(9);
		System.out.println(intQueue.element());
		
		System.out.println(intQueue.remove());
		System.out.println(intQueue);
		
		System.out.println(intQueue.poll());
		
		
	}
}
