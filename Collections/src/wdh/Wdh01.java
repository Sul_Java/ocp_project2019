package wdh;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class Wdh01 {
	
	static class OS implements Comparable<OS>{
		private String name;
		private int version;
		
		public OS(String name, int version) {
			this.name = name;
			this.version = version;
		}
		
		@Override
		public String toString() {
			return name + " " + version;
		}

		@Override
		public int compareTo(OS o) {
			return 0;
		}
		
		@Override
		public boolean equals(Object obj) {
			OS os2 = (OS) obj;
			return name.equals(os2.name) && version == os2.version;
		}
		
		@Override
		public int hashCode() {
			return name.hashCode() + version * 31;
		}
	}
	
	public static void main(String[] args) {
		
		List<OS> list = Arrays.asList(
				new OS("Mac", 2),
				new OS("Windows", 95),
				new OS("Mac", 2),
				new OS("Linux", 5)
				);
		
		Deque<OS> deque = new ArrayDeque<>(list);
		System.out.println(deque.size());
		
		Set<OS> hashSet = new HashSet<>(list);
		System.out.println(hashSet.size());
		
		Set<OS> tSet = new TreeSet<>(list);
		System.out.println(tSet.size());
		
		Queue<OS> queue = new PriorityQueue<>(list);
		System.out.println(queue.peek().name);
		
	}

}
