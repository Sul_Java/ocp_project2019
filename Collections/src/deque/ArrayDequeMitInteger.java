package deque;

import java.util.ArrayDeque;

public class ArrayDequeMitInteger {
	
	public static void main(String[] args) {
		
		ArrayDeque<Integer> deque = new ArrayDeque<>();
		
		boolean result = deque.add(7);
		System.out.println(result);
		System.out.println(deque.add(3));
		
		System.out.println(deque);
		
		result = deque.offer(11);
		System.out.println(result);
		System.out.println(deque.offer(5));
		
		System.out.println(deque);
		
		System.out.println(deque.element());
		System.out.println(deque.peek());
		
		System.out.println(deque.remove());
		System.out.println(deque.poll());
		
		System.out.println(deque);
		
		//**************************
		
//		System.out.println(deque.push(2));
		deque.push(2);
		System.out.println(deque);
		
		System.out.println(deque.pop());
		System.out.println(deque);
		
		System.out.println(deque.peek());
		System.out.println(deque);
		
		System.out.println(deque.offerFirst(16));
		deque.addFirst(77);
		deque.addLast(101);
		System.out.println(deque.getFirst());
		
	}

}
