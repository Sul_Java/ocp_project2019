package list;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class StackIstNichtEmpfohlen {
	
	public static void main(String[] args) {
		
		Map<String , Integer> map = new HashMap<>();
		
		map.put("mo" , 1000);
		map.put("di" , 2000);
		map.put("mi" , 3);
		
		BiConsumer<String, Integer> action = (Key , value) -> {
			System.out.println(Key + " " + value);
		};
		map.forEach(action);
		
		Set<String> allKey = map.keySet();
		System.out.println(allKey);
	}

}