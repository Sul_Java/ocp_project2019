package list;

import java.util.ArrayList;
import java.util.List;

public class UseLinkedList {
	
	public static void main(String[] args) {
		
		List<Character> list = new ArrayList<>();
		
		for (char ch = 'a'; ch < 'z'; ch++) {
			list.add(ch);
		}
		
		System.out.println(list);
		
		List<Character> list2 = list.subList(0, 10);
		System.out.println(list2);
		System.out.println(list);
		
		list.set(0, 'A');
		System.out.println(list);
		System.out.println(list2);
		
	}

}
