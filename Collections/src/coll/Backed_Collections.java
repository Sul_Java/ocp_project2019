package coll;

import java.util.Arrays;
import java.util.List;

public class Backed_Collections {
	
	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(12,13,14);
		
		Integer[] array = { 2, 3, 4 };
		list = Arrays.asList(array);
		
		System.out.println(Arrays.toString(array));
		System.out.println(list);
		
		array[0] = 5;
		array = null;
		
		System.out.println(list);
		
	}

}
