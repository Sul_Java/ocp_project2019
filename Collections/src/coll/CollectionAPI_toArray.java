package coll;

import java.util.Collection;
import java.util.LinkedList;

public class CollectionAPI_toArray {
	
	public static void main(String[] args) {
		
		Collection<Integer> coll = new LinkedList<>();
		coll.add(12);
		coll.add(13);
		coll.add(14);
		
		Object[] arr1 = coll.toArray();
		System.out.println(arr1.getClass().getSimpleName());
		
		Integer x = (Integer) arr1[1];
		System.out.println(x);
		
	}

}