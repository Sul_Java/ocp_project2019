package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
	
	public static void main(String[] args) {
		
//		Class.forName("com.mysql.cj.jdbc.Driver");
		
		String url = "jdbc:mysql://localhost?serverTimezone=UTC";
		String user = "root";
		String password = "Sulaiman@0788692504";
		
		try(Connection connection = DriverManager.getConnection(url, user, password) ){
			
			try(Statement stm = connection.createStatement() ){
				
				String sql = "SELECT * FROM java_test_db.personen";
				
				try( ResultSet res = stm.executeQuery(sql) ){
					
					while (res.next()) {
						
						int id = res.getInt(1);
						
						System.out.println(id);
					}
					
//					res.close();
				}
				
//				stm.close();
			}
//			connection.close();
		}catch (SQLException e) {
			System.out.println(e);
		}
		
		
	}

}
