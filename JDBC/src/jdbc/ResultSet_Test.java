package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultSet_Test {
	
	public static void main(String[] args) throws SQLException{
		
		MySqlUtils.removeTableTiere();
		MySqlUtils.buildTableTiere();
		
		Connection c = MySqlUtils.getConnection();
		Statement stm = c.createStatement();
				
		ResultSet res = stm.executeQuery("SELECT * FROM tiere");
		
		stm.close();
		
		res.next();
		
	}

}
