package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Statement_execute {
	
	public static void main(String[] args) throws SQLException {
		
		MySqlUtils.removeTableTiere();
		MySqlUtils.buildTableTiere();
		
		try( Connection c = MySqlUtils.getConnection();
				Statement stm = c.createStatement() ){
			
			int result = stm.executeUpdate("INSERT INTO `tiere` (`id`, `name`, `alter`)	VALUES (4, 'Rex', 2)");
			
			ResultSet res = stm.executeQuery("SELECT * FROM tiere");
			res.close();
			
			
			boolean res2 = stm.execute("INSERT INTO `tiere` (`id`, `name`, `alter`)	VALUES (5, 'Flipper', 7)");
			
			
			MySqlUtils.printTableTiere();
			
			stm.execute("SELECT * FROM tiere");
			
			
			
		}
		
	}

}
