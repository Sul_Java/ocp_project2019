package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

class Tier {
	private int id;
	private String name;
	private int alter;
	
	public Tier(int id, String name, int alter) {
		this.id = id;
		this.name = name;
		this.alter = alter;
	}
	
	@Override
	public String toString() {
		return name + " id: " + id + ", alter: " + alter;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAlter() {
		return alter;
	}
	
	
}

class Tiers {
	public static TierDAO getDefaultDAO() {
		return new MySqlTierDAO();
	}
}

interface TierDAO {
	void create(Tier t);
	void delete(int id);
	void update(Tier t);
	Tier findByID(int id);
	List<Tier> getAllTiere();
}

class MySqlTierDAO implements TierDAO{
	
	public static Connection getConnection() throws SQLException {
		String url = "jdbc:mysql://localhost/java_test_db?serverTimezone=UTC";
		return DriverManager.getConnection(url, "root", "Sulaiman@0788692504");
	}

	@Override
	public void create(Tier t)  {
		
		try( Connection c = getConnection() ){
			try(Statement stm = c.createStatement() ){
				stm.executeUpdate("insert into tiere values (" + t.getId() + ","+ t.getName() +"," + t.getAlter() + ")" );
			} 
		} catch (SQLException e) {
			System.out.println("Fehler beim insert");
		}
		
	}

	@Override
	public void delete(int id) {
		
	}

	@Override
	public void update(Tier t) {
		
	}

	@Override
	public Tier findByID(int id) {
		return null;
	}

	@Override
	public List<Tier> getAllTiere() {
		return null;
	}
	
}

public class DAO_Design_Pattern {

	public static void main(String[] args) {

		TierDAO dao = Tiers.getDefaultDAO();
		
		Tier t = new Tier(4, "Rex", 2);
		
		dao.create( t );
		
		dao.delete( 2 );

		dao.update( new Tier(1, "Tomas", 9) );
		
		System.out.println( dao.findByID(1) );

		List<Tier> alleTiere = dao.getAllTiere();
		
		System.out.println("*** alle Tiere: ");
		alleTiere.forEach(System.out::println);
	}

}