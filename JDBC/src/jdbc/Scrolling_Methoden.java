package jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Scrolling_Methoden {
	
	public static void main(String[] args) throws SQLException {
		
		MySqlUtils.removeTableTiere();
		MySqlUtils.buildTableTiere();
		MySqlUtils.printTableTiere();
		
		int resultSetType = ResultSet.TYPE_SCROLL_SENSITIVE;
		int resultSetConcurrency = ResultSet.CONCUR_READ_ONLY;
		
		try( Connection c = MySqlUtils.getConnection();
				Statement stm = c.createStatement(resultSetType , resultSetConcurrency);
				ResultSet res = stm.executeQuery("SELECT * FROM tiere")){
			
			res.next();
			System.out.println("1. " + res.getString(2));
			
			res.last();
			System.out.println("2. " + res.getString(2));
			
			res.first();
			System.out.println("3. " + res.getString(2));
			
			res.beforeFirst();
			
			res.afterLast();
			
			System.out.println("alle zeile r�ckw�rts");
			
			while (res.previous()) {
				System.out.println(res.getString(2));
			}
			
			res.absolute(-3);
			System.out.println("4. " + res.getString(2));
		}
		
		
	}

}
