# SQL, MySql

## Datenbank erstellen

	CREATE SCHEMA `java_test_db` ;

## Tabelle erzeugen

	CREATE TABLE `java_test_db`.`personen` (
 	 `id` INT NOT NULL AUTO_INCREMENT,
 	 `vorname` VARCHAR(45) NULL,
	  `nachname` VARCHAR(45) NULL,
  	`geburtsjahr` INT NULL,
 	 PRIMARY KEY (`id`),
 	 UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);