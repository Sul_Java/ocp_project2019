package generics;

import java.util.Arrays;
import java.util.Random;

public class OhneGenerics {
	
	public static void main(String[] args) {
		
		Object[] arr = {
				12,
				"22",
				5
		};
		
		if (new Random().nextInt(2) == 1) {
			Arrays.sort(arr);
		}
		
		System.out.println(arr[0]);
	}

}
