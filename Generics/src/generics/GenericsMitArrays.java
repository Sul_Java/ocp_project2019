package generics;

public class GenericsMitArrays {
	
	static class Test <T> {
		
		@SuppressWarnings("unchecked")
		void m1(T... arr) {
		}
		
		void m2(T e1 , T e2) {
			T[] arr1 = null;
			
			boolean result = e1 instanceof Integer;
		}
		
	}
	
	public static void main(String[] args) {
		
		Test<Integer> var1 = new Test<Integer>();
		
		var1.m1(1 , 2 , 3 , 4);
		
	}

}
