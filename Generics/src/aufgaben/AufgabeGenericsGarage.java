package aufgaben;

interface Fahrzeug{}

class PKW implements Fahrzeug{}
class LKW implements Fahrzeug{}

class Garage<T extends Fahrzeug>{
	
	private T fahrzeug;
	
	public void reinfahren(T fahrzeug) {
		this.fahrzeug = fahrzeug;
	}
	
}

public class AufgabeGenericsGarage {
	
	public static void main(String[] args) {
		
		PKW pkw = new PKW();
		LKW lkw = new LKW();
		
		Garage<LKW> garageLKW = new Garage<>();
		Garage<PKW> garagePKW = new Garage<>();
		
		garagePKW.reinfahren(pkw);
		garageLKW.reinfahren(lkw);
		
		
	}

}
