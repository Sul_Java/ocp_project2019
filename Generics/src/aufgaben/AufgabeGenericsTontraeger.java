package aufgaben;

interface Tontraeger {}

class Schallplatte implements Tontraeger {}
class Tonband implements Tontraeger {}
class CD implements Tontraeger {}

class Abspielgeraet <T extends Tontraeger>{
    void abspielen(T t) {}
}

public class AufgabeGenericsTontraeger {
	
	public static void main(String[] args) {
		
		Schallplatte schaltplatte = new Schallplatte();
		
		Tonband tonband = new Tonband();
		
		CD cd = new CD();
		
		Abspielgeraet<Schallplatte>  schaltplatteSpieler = new Abspielgeraet<>();
		schaltplatteSpieler.abspielen(schaltplatte);
		
		Abspielgeraet<CD> cdSpiler = new Abspielgeraet<>();
		cdSpiler.abspielen(cd);
		
		
		
	}

}
