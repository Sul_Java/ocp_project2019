package steckdosen.loesung;


/*
* Aufgabe:
* 
* Typensammlung soll entstehen, die folgende Bedingungen erfüllt
* 
* - Es soll Steckdosen geben können
* - Es soll auch TV-Geräte geben können
* - TV-Geräte sollen an Steckdosen angeschlossen werden können
* 
* - Wichtig! Es gibt unterschiedlichen Standards für die Steckdosen (und TV-Geräte): englsche und deutsche Steckdosen
*   
*   englische TV-Geräte können nur an englische Steckdosen angeschlossen werden 
*   (und deutsche TV-Geräte nur an die deutschen Steckdosen) 
*  
*
*/

interface TV {}
class TVDE implements TV {}
class TVUK implements TV {}


class Steckdose <T extends TV> {  // T extends TV ist 'Typebound' um nur sinnvolle Parametrisierungen zu erlauben
	private T tv;
	
	void anschliessen(T tv) {
		this.tv = tv;
	}
	
	T get() {
		return tv;
	}
}


public class LoesungMitGenerics {

	public static void main(String[] args) {
		
		TVDE tvDE = new TVDE();
		TVUK tvUK = new TVUK();

		Steckdose<TVDE> sdDE = new Steckdose<>();
		
		sdDE.anschliessen(tvDE); // muss gehen
//		sdDE.anschliessen(tvUK); // darf nicht kompilieren

		Steckdose<TVUK> sdUK = new Steckdose<>();
		// sdUK.anschliessen(tvDE);
		sdUK.anschliessen(tvUK);

//		 Steckdose<String> sdSinnlos = new Steckdose<>(); // Sinnlose Steckdosen sollten nicht möglich sein (mit Typebound ausgeschlossen)
//		 sdSinnlos.anschliessen("Ich bin kein Fernseher");
		
	}

}
