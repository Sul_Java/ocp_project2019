package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

public class BS_Iterable {
	
	public static void main(String[] args) {
		
		Iterable<Integer> it = Arrays.asList(11 , 12 , 13);
		
		it.forEach(System.out::println);
		
		System.out.println("***********************");
		
		Collection<Integer> coll = Arrays.asList(12 , 13 ,14);
		
		Predicate<Integer> filter = x -> x%2 != 0;
		
		coll = new ArrayList<Integer>(coll);
		
		
		coll.removeIf(filter);
		
		coll.forEach(System.out::println);
		
		
		
	}

}
