package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class BS_Map {
	
	public static void main(String[] args) {
		forEach();
		replaceAll();
		merge();
		
	}
	
	static void merge() {
		Map<Integer, String> map = getWochenMap();
		
		String value = "irgendwas";
		
		BiFunction<String, String, String> remappingFunction = (s1 ,s2) -> s1+s2;
		String m = map.merge(8, value, remappingFunction);
		
		System.out.println(m);
		System.out.println(map);
		
	}

	static void forEach() {
		
		Map<Integer, String> map = getWochenMap();
		
		BiConsumer<Integer, String> action = (k , v) -> System.out.println(k + " = " + v);
		map.forEach(action);
	}

	static void replaceAll() {
		
		Map<Integer, String> map = getWochenMap();
		
		System.out.println(map);
		
		BiFunction<Integer, String, String> function = (key , oldValue) -> {
			String newValue = oldValue.toUpperCase();
			return newValue;
		};
		map.replaceAll(function);
		
		System.out.println(map.get(1));
		
	}
	
	static Map<Integer, String> getWochenMap(){
		Map<Integer, String> map = new HashMap<>();
		map.put(1, "mo");
		map.put(2, "di");
		map.put(3, "mi");
		map.put(4, "do");
		map.put(5, "fr");
		map.put(6, "sa");
		map.put(7, "so");
		return map;
	}
}
