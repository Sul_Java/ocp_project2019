package collections;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;

public class BS_List {
	
	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(12 , 13 ,14);
		
		list.sort(Comparator.reverseOrder());
		
		list.forEach(System.out::println);
		
		System.out.println("***********");
		
		UnaryOperator<Integer> operator = x -> x+1;
		
		list.replaceAll(operator);
		
		list.forEach(System.out::println);
		
	}

}
