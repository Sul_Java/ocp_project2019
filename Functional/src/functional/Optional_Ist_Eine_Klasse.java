package functional;

import java.util.Optional;
import java.util.function.Supplier;

public class Optional_Ist_Eine_Klasse {
	
	static String getDayName(int dayNumber) {
		switch (dayNumber) {
			case 1: return "mo";
			case 2: return "di";
			//...
		}
		return null;
	}
	
	static Optional<String> maybeGetDayName(int dayNumber) {
		switch (dayNumber) {
			case 1: return Optional.of("mo");
			case 2: return Optional.of("di");
			//...
		}
		return Optional.empty();
	}
	
	public static void main(String[] args) throws Exception {
		
		String day1 = getDayName(22);
		if (day1 != null) {
			System.out.println(day1.toUpperCase());
		}
		
		Optional<Integer> op = Optional.empty();
		
		Supplier<Integer> supplier = () -> 5;
		Integer result = op.orElseGet(supplier);
		System.out.println(result);
		
		Optional<Integer> op2 = Optional.empty();
		Supplier<IllegalArgumentException> exceptionSupplier = () -> new IllegalArgumentException("war leer");
		
		try {
			Integer result2 = op2.orElseThrow(exceptionSupplier );
			System.out.println(result2);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		op2.orElseThrow(() -> new Exception());
		
	}

}
