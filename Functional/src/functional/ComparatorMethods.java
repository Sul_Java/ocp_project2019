package functional;

import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

class OS implements Comparable<OS>{
	
	public static final OS LAST_WIND = new OS("Windows", 2010 , 12);
	public static final OS LAST_LINUX = new OS("Linux", 3 , 9);
	private String name;
	private int major , minor;
	
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name + major + "." + minor;
	}

	public OS(String name, int major, int minor) {
		this.name = name;
		this.major = major;
		this.minor = minor;
	}

	public OS(String name, int major) {
		this(name, major, 0);
	}

	@Override
	public int compareTo(OS os2) {
		int erg = name.compareTo(os2.name);
		if (erg == 0) {
			erg = major - os2.major;
		}
		if (erg == 0) {
			erg = minor - os2.minor;
		}
		return erg;
	}
}
public class ComparatorMethods {

	public static void main(String[] args) {
		
		Comparator<Integer> c1 = new Comparator<Integer>() {
			@Override
			public int compare(Integer i1, Integer i2) {
				return i1 - i2;
			}
		};
		
		Comparator<Integer> c2 = Collections.reverseOrder();
		
		Comparator<Integer> c3 = Collections.reverseOrder(c1);
		System.out.println(c3.compare(5, 7));
		
		Comparator<Integer> c4 = Comparator.naturalOrder();
		
		Comparator<OS> c5 = Comparator.naturalOrder();
		
		Comparator<OS> c6 = c5;
		int result = c6.compare(OS.LAST_WIND, OS.LAST_LINUX);
		System.out.println(result);
		
		
		Function<OS , String> keyExtractor = os -> os.getName();
		Comparator<OS> c7 = Comparator.comparing(keyExtractor);
		
		OS tmp = new OS("Linux", 2);
		
		System.out.println(c7.compare(tmp, OS.LAST_LINUX));
		System.out.println(c7.compare(tmp, OS.LAST_WIND));
		
		Comparator<String> keyComparator = Comparator.reverseOrder();
		Comparator<OS> c8 = Comparator.comparing(keyExtractor, keyComparator);
		System.out.println(c8.compare(tmp, OS.LAST_WIND));
		
		
		ToDoubleFunction<String> keyExtractorDouble = s -> Double.parseDouble(s);
		Comparator<String> c9 = Comparator.comparingDouble(keyExtractorDouble);
		
		System.out.println("*****************");
		
		System.out.println(c9.compare("22.3", "5.7"));
		
	}
}


