package functional;

import java.util.Comparator;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;

class MyUtils1{
	static boolean isNotLeer(String s) {
		return false;
		
	}

	public static int random(Number n1 , Number n2) {
		return new Random().nextInt();
	}
}

public class Method_Referece {
	
	public static void main(String[] args) {
		
		
		Function<String, Integer> f0 = new Function<>() {
			@Override
			public Integer apply(String s) {
				return s.length();
			}
		};
		Function<String , Integer> f1 = s -> s.length();
		
		Function<String, Integer> f2 = String::length;
		
		kleineAufgabe2();
		
	}
	
	static void kleineAufgabe() {
		
		Comparator<Integer> c1 = MyUtils1::random;
		
	}

	static void kleineAufgabe2() {
		
		BiFunction<String, String, String> f0 = new BiFunction<>() {
			@Override
			public String apply(String s1, String s2) {
				return s1.concat(s2);
			}
		};
		
		BiFunction<String, String, String> f1 = (s1 , s2) -> s1.concat(s2);
		BiFunction<String, String, String> f2 = String::concat;
		
		System.out.println(f2.apply("a", "bc"));
	}
}
