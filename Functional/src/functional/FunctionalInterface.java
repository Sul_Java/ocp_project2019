package functional;

import java.util.function.Consumer;
import java.util.function.Function;

public class FunctionalInterface {
	
	public static void main(String[] args) {
		
		Function<String,Function<String ,String>> v5 = s -> x -> x.concat(s);
		
		System.out.println(v5.apply("s"));
		
		Function<String,Consumer<String>> v6 = new Function<String, Consumer<String>>() {
			@Override
			public Consumer<String> apply(String s) {
				return new Consumer<String>() {
					@Override
					public void accept(String x) {
						x.concat(s);
					}
				};
			}
		};
		
		Consumer<String> c2 = v6.apply("s");
		v6.apply("a").accept("b");
		
	}

}
