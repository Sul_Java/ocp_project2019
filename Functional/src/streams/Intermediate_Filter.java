package streams;

import java.util.stream.Stream;

public class Intermediate_Filter {
	
	public static void main(String[] args) {
		
//		Predicate<Integer> predicate = x -> x == 0;
		
		Stream.of(1 , 2 , 3 , 0 , 5 , 0 , 7)
			  .filter(x -> x != 0 )
			  .forEach(System.out::println);
		
		kleineAufgabe();
	}

	static void kleineAufgabe() {
		
		String[] arr = { "mo" , "di" , "mi" , "do" , "fr" };
		
		Stream.of(arr)
			.filter( s -> s.indexOf('m') != -1)
			.forEach(System.out::println);
		
	}
}
