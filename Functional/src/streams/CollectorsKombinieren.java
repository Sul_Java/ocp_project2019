package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorsKombinieren {
	
	static class Kreis{
		int radius;

		public Kreis(int radius) {
			this.radius = radius;
		}
		@Override
		public String toString() {
			return "Kreis " + radius;
		}
		
	}
	
	public static void main(String[] args) {
		
		Integer[] radien = {1 , 2 , 3 , 4 , 5};
		
		Collector<Kreis, ?, Map<String, List<Kreis>>> c1 = Collectors
												.groupingBy(k -> k.radius % 2 == 0 ? "gerade":"ungerade");
		
		Map<String, List<Kreis>> map1 = Arrays.stream(radien).map(Kreis::new).collect(c1);
		System.out.println("Kreis mit geraden Radius: " +map1.get("gerade"));
		System.out.println("Kreis mit ungeraden Radius: " +map1.get("ungerade"));
		
//		Collector c1 = null;
//		Collector c2 = Collectors.mapping(mapper, c1);
//		Collector c3 = Collectors.groupingBy(classifier ,c2);
		
	}

}
