package streams;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

public class Intermediate_map_sorted_flatMap {
	
	public static void main(String[] args) {
		
		Function<Integer , String> mapper = i -> "<" + i + ">" ;
		Stream.of(4 , 2 , 3 , 2 , 1)
			  .map(mapper )
//			  .sorted((i1 ,i2) -> i1.compareTo(i2) )
			  .forEach(System.out::println);
		
		
		
		kleineAufgabe();
	}

	static void kleineAufgabe() {
		System.out.println("Aufgabe");
		
		class Person{
			String name;

			public Person(String name) {
				this.name = name;
			}
			@Override
			public String toString() {
				return "Person " + name;
			}
		}
		
		String[] namen = {
			"Peter" , "Paul" , "Mary"	
		};
		
		Arrays.stream(namen)
			.map(name -> new Person(name) )
			.forEach(System.out::println);;
	}
}
