package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class ReductionOperations {
	
	public static void main(String[] args) {
//		reduce1();
//		reduce2();
		reduce3();
	}
	
	
	static void reduce3() {
		
		Double[] zahlen = {1.3 , -2000.77 , 5.9 , -33.1 , 8. }; 
		
		System.out.println(Arrays.stream(zahlen).filter(x -> x<0).count());
		
		Integer identity = 0;
		BiFunction<Integer , Double , Integer> accumulator = (count , zahl) -> {
			if (zahl < 0) {
				count++;
			}
			return count;
		};
		BinaryOperator<Integer> combiner = (countA ,countB) -> countA + countB;
		Arrays.stream(zahlen).reduce(identity, accumulator ,combiner);
		
	}


	static void reduce2() {
		
		List<Integer> list = Arrays.asList(1 , 2 , 3 , 4);
		
		BiFunction<Integer , Integer , Integer> accumulator = (x1,x2) -> x1 + x2;
		Integer identity = 0;
		BinaryOperator<Integer> combiner = (x1,x2) -> x1 + x2;
		
		Integer sum = list.stream()
						.parallel()
						.reduce(identity, accumulator, combiner);
		System.out.println(sum);
	}


	
	static void reduce1() {
		
		List<Integer> list = Arrays.asList(1 , 2 , 3 , 4);
		
		BinaryOperator<Integer> accumulator = (x1,x2) -> x1+x2;
		Integer identity = 0;
		Integer sum = list.stream().reduce(identity , accumulator );
		System.out.println(sum);
		
		System.out.println(Stream.of(77).reduce(1 ,accumulator));
		System.out.println(Stream.<Integer>empty().reduce(33 ,accumulator));
		
		
	}

}
