package streams;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

public class Terminal_count_min_max {

	public static void main(String[] args) {
		
		long result = Stream.of(1 , 2 , 3 , 4 , 5 , 6)
						.filter(x -> x%2 == 0)
						.count();
		
		System.out.println("result = "+result);
		
		Comparator<Integer> comparator = Comparator.naturalOrder();
		
		Optional<Integer> min = Stream.of(1 , 3 , 2 , 5 , -7 )
									.min(comparator );

		System.out.println("min: "+min.get());
		
		Optional<Integer> max = Stream.of(1 , 3 , 2 , 5 , -7 )
				.max(comparator );

		System.out.println("max: "+max.get());
		
	}

}
