package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Pipeline {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(1 , 2 , 3);
		
		list.stream()
			.map(x -> x+1)
			.map(x -> x*2)
			.forEach(System.out::println);
		
		System.out.println(list);
		
		Integer seed = 1;
		UnaryOperator<Integer> operator = i -> i+1;
		
		Stream.iterate(seed, operator).limit(4).forEach(System.out::println);
		
		System.out.println("**********************");
		
		Stream<Integer> s1 = Stream.of(1 , 2);
		Stream<Integer> s2 = Stream.of(3 , 4 , 5);
		
		Stream.concat(s1, s2)
			  .parallel()
			  .forEach(System.out::println);
		
	}
}