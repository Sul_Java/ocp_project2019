package streams;

import java.util.stream.Stream;

public class Terminal_allMatch_anyMatch_noneMatch {
	
	public static void main(String[] args) {
		
		boolean b1 = Stream.of(1 , 2 , 3 , 12 , 1 , -9 , 2)
			.noneMatch(x -> x >0);
		System.out.println(b1);
		
		boolean b2 = Stream.of(1 , 2 , 3 , 12 , 1 , -9 , 2)
				.anyMatch(x -> x >0);
		System.out.println(b2);
			
		boolean b3 = Stream.of(1 , 2 , 3 , 12 , 1 , -9 , 2)
				.allMatch(x -> x >0);
		System.out.println(b3);
		
	}

}
