package streams;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CollectorsMapping {
	
	public static void main(String[] args) {
		
		kleineAufgabe();
		
	}
	
	static void kleineAufgabe() {
		
		Integer[] zahlen = {1000000000 , 22 , 3330000 , 4444};
		
		Function<Integer , String> mapper = String::valueOf;
		Collector<String , ? , TreeSet<String>> downstream = Collectors.toCollection(TreeSet::new);
		
		Collector<Integer , ? , TreeSet<String>> collector= Collectors.mapping(mapper, downstream);
		
		TreeSet<String> set = Arrays.stream(zahlen).collect(collector);
		System.out.println(set);
		
	}

}
