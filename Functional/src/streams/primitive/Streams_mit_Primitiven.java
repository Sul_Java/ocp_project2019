package streams.primitive;

import java.time.LocalDate;
import java.util.OptionalDouble;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Streams_mit_Primitiven {
	
	public static void main(String[] args) {
		
		Stream.of(LocalDate.now().minusDays(1) , LocalDate.now() , LocalDate.now().plusDays(1))
				.map(LocalDate::toString)
				.forEach(System.out::println);
		
		System.out.println("_____________________________________");
		
		IntStream.of(1,2,3)
					.asDoubleStream()
					.forEach(System.out::println);
		
		System.out.println("_____________________________________");
		
		OptionalDouble m = IntStream.of(1,2,3)
										.average();
		System.out.println(m);
		
		System.out.println("_____________________________________");
		
		
		
	}
	
}
