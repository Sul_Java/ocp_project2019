package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Terminal_collect {
	
	public static void main(String[] args) {
		
		Supplier<List<Integer>> supplier = ArrayList::new;
		BiConsumer<List<Integer> , Integer> accumulator = (List<Integer> list , Integer element) ->{
			list.add(element);
		};
		
		BiConsumer<List<Integer> , List<Integer>> combiner = (listA , listB) -> {
			listA.addAll(listB);
		};
		
		List<Integer> result = Stream.of(1 , 2 , 3 , 4 , 5).collect(supplier, accumulator, combiner);
		System.out.println(result);
		
		result = Stream.of(1 , 2 , 3 , 4 , 5).collect(ArrayList::new , List::add , List::addAll);
		System.out.println(result);
		
		System.out.println("collect v2 ******* toList ******");
		
		Integer[] array = {22 , 3 , 22 , 5 , 22 , -1 , 22};
		
		Collector<Integer, ?, List<Integer>> c1 = Collectors.toList();
		
		List<Integer> list1 = Arrays.stream(array).collect(c1);
//		List<Integer> list1 = Arrays.stream(array).collect(Collectors.toList());
		System.out.println(list1);
		
		System.out.println("**********");
		
		Collector<Integer, ?, Set<Integer>> c2 =Collectors.toSet();
		
		Set<Integer> set1 = Arrays.stream(array).collect(c2);
		System.out.println(set1);
		
		System.out.println("**********");
		
		Supplier<TreeSet<Integer>> collectionFactory = TreeSet::new ;
		
		Collector<Integer, ?, TreeSet<Integer>> c3 = Collectors.toCollection(collectionFactory);
		
		Arrays.stream(array).collect(c3);
		
	}
	
	static void toMap() {
		
		Integer[] arr = {22 , 3 ,22 , 5 , 22 , -1 , 22};
		
		Function<Integer ,Integer> keyMapper;
		
		Function<Integer ,String> valueMapper;
		
		Collectors.toMap(keyMapper, valueMapper);
		
	}

}
