package streams;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Collectors_partitioningBy {
	
	public static void main(String[] args) {
		
		Function<Integer, Boolean> classifier = x -> x%2 == 0;
		Collector<Integer, ?, Map<Boolean, List<Integer>>> collector = Collectors.groupingBy(classifier);
		
		Map<Boolean, List<Integer>> map = Stream.of(22 , -3 , 7 , -55 , 13 , 22).collect(collector);
		System.out.println(map);
		
		// ******************************************
		
		Predicate<Integer> predicate = x -> x%2 == 0;
		Collector<Integer, ?, Map<Boolean, List<Integer>>> c2 = Collectors.partitioningBy(predicate);
		
		Map<Boolean, List<Integer>> map2 = Stream.of(22 , -3 , 7 , -55 , 13 , 22).collect(collector);
		System.out.println(map2);
		
		// ******************************************
		
		Collector<Integer, ?, Set<Integer>> downstream = Collectors.toSet();
		Collector<Integer, ?, Map<Boolean, Set<Integer>>> c3 =Collectors.partitioningBy(predicate, downstream );
		 
		Map<Boolean, Set<Integer>> map3 = Stream.of(22 , -3 , 7 , -55 , 13 , 22).collect(c3);
		System.out.println(map3);
		
	}

}
