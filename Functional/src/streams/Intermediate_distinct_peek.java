package streams;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class Intermediate_distinct_peek {
	
	public static void main(String[] args) {
		
		Stream.of(1 , 12 , 3 , 12 , -7 , 1)
			.distinct()
			.forEach(System.out::println);
		
		Consumer<Integer> c = x -> {
			System.out.println("x = " + x);
		};
		Stream.of(1,2,3,4,5)
			.map(x -> ++x)
			.peek(c)
			.forEach(System.out::println);;
		
			
		Stream.of(1,2,3,4,5)
			.peek(c);
	}

}
