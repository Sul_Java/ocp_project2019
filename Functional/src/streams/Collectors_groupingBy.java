package streams;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Collectors_groupingBy {
	
	public static void main(String[] args) {
		
		groupingBy_v1();
//		
//		kleineAufgabe1();
//		
//		groupingBy_v2();
//		
//		kleineAufgabe2();
//		
//		groupingBy_v3();
		
//		kleineAufgabe3();
	}
	
	static void kleineAufgabe3() {
		
		Integer[] array = { 12 , -22 , 0 , 77 , 0 , -5 , -5 , 33 , 6};
		
		Function<Integer, String> classifier = num -> num > 0 ?"positive":num < 0 ? "negative":"zero";
		Supplier<TreeMap<String, Integer>> mapFactory = TreeMap::new;
		Collector<Integer, ?, TreeMap<String ,Integer>> downstream = null;
		
		Collector<Integer, ?, Map<String, TreeMap<String,Integer>>> collector = Collectors.groupingBy(classifier, mapFactory, downstream);
		
		Map<String, TreeMap<String,Integer>> map = Arrays.stream(array).collect(collector  );
		
	}

	static void groupingBy_v3() {
		
		Function<String, Integer> classifier = String::length;
		Supplier<LinkedHashMap<Integer, HashSet<String>>> mapFactory = LinkedHashMap::new;
		Collector<String, ?, HashSet<String>> downstream = Collectors.toCollection(HashSet::new);
		
		Collector<String, ?, LinkedHashMap<Integer, HashSet<String>>> collector = Collectors.groupingBy(classifier, mapFactory, downstream);
		
		Map<Integer, HashSet<String>> linkedHashMap = Stream.of("abc" ,"hallo" ,"fff" ,"xy" ,"welt" , "123" , "cc")
																.collect(collector);
		
		printGroup(linkedHashMap);
	}

	static void kleineAufgabe2() {
		
		Integer[] array = { 12 , -22 , 0 , 77 , 0 , -5 , -5 , 33 , 6};
		
		Function<Integer ,String> classifier = num -> num > 0 ?"positive":num < 0 ? "negative":"zero";
		Collector<Integer, ?, TreeSet<Integer>> downstream = Collectors.toCollection(TreeSet::new);
		
		Collector<Integer, ?, Map<String, TreeSet<Integer>>> collector = Collectors.groupingBy(classifier, downstream);
		Map<String, TreeSet<Integer>> map = Arrays.stream(array).collect(collector );
		
		printGroup(map);
		
	}

	static void groupingBy_v2() {
		
		Function<String, Integer> classifier = String::length;
		Collector<String, ?, HashSet<String>> downstream = Collectors.toCollection(HashSet::new);
		
		Collector<String, ?, Map<Integer, HashSet<String>>> collector = Collectors.groupingBy(classifier, downstream);
		
		Map<Integer, HashSet<String>> map = Stream.of("abc" ,"hallo" ,"fff" ,"xy" ,"welt" , "123" , "cc")
											.collect(collector);
		
		printGroup(map);
	}

	static void kleineAufgabe1() {
		
		Integer[] array = { 12 , -22 , 0 , 77 , 0 , -5 , -5 , 33 , 6};
		
		Function<Integer ,String> classifier = num -> num > 0 ?"positive":num < 0 ? "negative":"zero";
		Collector<Integer, ?, Map<String, List<Integer>>> collector = Collectors.groupingBy(classifier  );
		
		Map<String, List<Integer>> map = Arrays.stream(array).collect(collector);
		
		printGroup(map);
		}
	static <T> void printGroup(Map<?, ? extends Collection<T>> map) {
		
		Set<?> gruppID = map.keySet();
		for (Object id : gruppID) {
			Collection<T> element =  map.get(id);
			System.out.println(id + " -> " + element);
		}
		
	}

	static void groupingBy_v1() {
		
		Function<String , Integer> classifier = str -> str.length();
		Collector<String, ?, Map<Integer, List<String>>> collector = Collectors.groupingBy(classifier );
		
		Map<Integer ,List<String>> map = Stream.of("abc" ,"hallo" ,"fff" ,"xy" ,"welt" , "123" , "cc")
												.collect(collector);
		
		printGroup(map);
		
	}

}
