package streams;

import java.util.Arrays;
import java.util.List;

public class ParaallelStreams {
	
	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(12, 14, 16, 18);
		
		Integer result = list.parallelStream()
							.filter(x -> {
								System.out.println(x);
								return true;
							})
							.findFirst()
							.get();
		System.out.println(result);
		
	}

}
