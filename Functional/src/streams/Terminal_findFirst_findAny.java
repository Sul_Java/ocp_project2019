package streams;

import java.util.Optional;
import java.util.stream.Stream;

public class Terminal_findFirst_findAny {
	
	public static void main(String[] args) {
		
		Optional<Integer> findfirst = Stream.of(1 , 2 , 5 , 9 , 7 , 9 , 1)
											.parallel()
											.filter(i -> i > 4)
											.findFirst();
		System.out.println(findfirst.get());
		
		Optional<Integer> findany = Stream.of(1 , 2 , 5 , 9 , 7 , 9 , 1)
											.parallel()
											.filter(i -> i > 4)
											.findAny();
		System.out.println(findany.get());
		
		
	}

}
