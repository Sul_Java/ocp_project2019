package streams;

import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Intermediate_limit_skip {
	
	public static void main(String[] args) {
		
		long max = 5;
		UnaryOperator<Integer> op = x -> x+1;
		
		Stream.iterate(1, op)
			.limit(max)
			.forEach(System.out::println);
		
		System.out.println("******** skip *******");
		
		long n = 3;
		
		Stream.iterate(1, op)
			.skip(n)
			.limit(3)
			.forEach(System.out::println);
		
	}

}
