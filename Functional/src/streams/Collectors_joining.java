package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Collectors_joining {
	
	public static void main(String[] args) {
		
		List<String> list = Arrays.asList("a","b" ,"c" ,"d" ,"e");
		
		Collector<CharSequence, ?, String> collector =Collectors.joining();
		String s = list.stream().collect(collector );
		System.out.println(s);
		
		CharSequence delimeter = ", ";
		Collector<CharSequence, ?, String> collector2 =Collectors.joining(delimeter);
		
		String s2 = list.stream().collect(collector2);
		System.out.println(s2);
		
		CharSequence suflix = " >";
		CharSequence prefix = " < ";
		CharSequence delimeter2 = ", ";
		Collector<CharSequence, ?, String> collector3 =Collectors.joining(delimeter2  , prefix , suflix);
		
		String s3 = list.stream().collect(collector3);
		System.out.println(s3);
		
	}

}
