package aufgaben.streams;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class KleineAufgabeStreamBuilden {

	public static void main(String[] args) {
		
		String[] a1 = {"mo" , "di"};
		String[] a2 = {"mi" , "do" , "fr"};
		
		Consumer<String[]> c = arr -> {
			for (String s : arr) {
				System.out.print(s +" ");
			}
			System.out.println();
		};
		Stream.of(a1,a2)
			  .forEach(c);
			  
		Stream.<String[]>of(a1).forEach(System.out::println);
		
	}
}
