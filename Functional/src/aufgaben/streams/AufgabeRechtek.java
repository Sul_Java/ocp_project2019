package aufgaben.streams;

import java.util.Comparator;
import java.util.stream.Stream;

class Rechtek implements Comparable<Rechtek> {
	int breite, hoehe;

	public Rechtek(int breite, int hoehe) {
		this.breite = breite;
		this.hoehe = hoehe;
	}
	
	@Override
	public String toString() {
		return "Rechtek : (" + breite +" , " + hoehe +")";
	}
	
	@Override
	public int compareTo(Rechtek o) {
		int erg = breite - o.breite;
		if (erg == 0) {
			erg = hoehe - o.hoehe;
		}
		return erg;
	}

}

public class AufgabeRechtek {
	 
	public static void main(String[] args) {
		
		Rechtek r1 = new Rechtek(10, 20);
		Rechtek r2 = new Rechtek(30, 40);
		Rechtek r3 = new Rechtek(10, 10);
		Rechtek r4 = new Rechtek(30, 40);
		Rechtek r5 = new Rechtek(70, 100);
		
		Rechtek[] arr = {
			r1 , r2 , r3 , r4 , r5	
		};
		
		Comparator<Rechtek> cmp = (ob1 ,ob2) -> {
			int erg = ob1.breite - ob2.breite;
			if (erg == 0) {
				erg = ob1.hoehe - ob2.hoehe;
			}
			return erg;
		};
		Stream.of(arr)
			  .sorted(cmp)
			  .forEach(System.out::println);
		
	}

}
