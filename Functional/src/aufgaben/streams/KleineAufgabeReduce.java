package aufgaben.streams;

import java.util.Arrays; 
import java.util.function.BinaryOperator;

public class KleineAufgabeReduce {
	
	public static void main(String[] args) {
		
		a1();
		a2();
		a3();
		
	}
	
	
	static void a3() {
		
		Integer[] zahlen = { 1 , 2 , 3 , 4 };
		
		BinaryOperator<Integer> op  = (a , b) -> a * b;
		Integer multi = Arrays.stream(zahlen).reduce(1 , op , op);
		System.out.println(multi);
		
	}


	static void a2() {
		
		Integer[] zahlen = { 1 , 2 , 3 , 4 };
		
		BinaryOperator<Integer> accumulator  = (a , b) -> a * b;
		Integer multi = Arrays.stream(zahlen).reduce(1 ,accumulator );
		System.out.println(multi);
	}

	static void a1() {
		
		Integer[] zahlen = { 1 , 2 , 3 , 4 };
		
		BinaryOperator<Integer> accumulator = (a , b) -> a * b;
		Integer multi = Arrays.stream(zahlen).reduce(accumulator ).get();
		System.out.println(multi);
	}

}
