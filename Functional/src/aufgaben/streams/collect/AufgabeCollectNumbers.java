package aufgaben.streams.collect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class AufgabeCollectNumbers {
	
	public static void main(String[] args) {
		
		String[] arr = {
		        "1,2,3,4,5",
		        "7,6,5,4,3",
		        "123,456",
		    };

		    List<Integer> list;

		    // A
		    list = new ArrayList<>();
		    for (String s : arr) {
		        String[] stringNumbers = s.split(",");

		        for (String sNum : stringNumbers) {
		            Integer num = Integer.valueOf(sNum);
		            list.add(num);
		        }
		    }
		    // B

		    System.out.println(list); // [1, 2, 3, 4, 5, 7, 6, 5, 4, 3, 123, 456]
		    System.out.println("***** A1 *****");
		    
		    a1a(arr);
		    a1b(arr);
		    a1c(arr);
		    
		    a2(arr);
		    

	}
	
	static void a2(String[] arr) {
		System.out.println("******* A2 ********");
		
		List<Integer> list = Arrays.stream(arr)
				.map(str -> str.split(","))
				.flatMap( Arrays::stream)
				.map(Integer::valueOf)
				.filter(x -> x%2 == 0)
				.collect(ArrayList::new, List::add, List::addAll);

		System.out.println(list); //[2, 4, 6, 4, 456]
		
	}
	
	static void a1c(String[] arr) {
		System.out.println("****** A1c Methodreferences******");
	    
		List<Integer> list = Arrays.stream(arr)
								.map(str -> str.split(","))
								.flatMap( Arrays::stream)
								.map(Integer::valueOf)
								.collect(ArrayList::new, List::add, List::addAll);
		
	    System.out.println(list);
		
	}
	
	static void a1b(String[] arr) {
		System.out.println("****** A1b ******");
	    Supplier<List<Integer>> supplier = () -> new ArrayList<>();
	    BiConsumer<List<Integer>, Integer> accumulator = (list2 , element) -> list2.add(element);
	    BiConsumer<List<Integer>, List<Integer>> combiner = (listA , listB) -> listA.addAll(listB);
	    
		List<Integer> list = Arrays.stream(arr)
								.map(str -> str.split(","))
								.flatMap( strArr -> Arrays.stream(strArr))
								.map(str -> Integer.valueOf(str))
								.collect(supplier, accumulator, combiner);
		
	    System.out.println(list);
		
	}
	static void a1a(String[] arr) {
//	    Supplier<List<Integer>> supplier = () -> new ArrayList<>();
	    BiConsumer<List<Integer>, String> accumulator = (list2 , element) -> {
	    	String[] stringNumbers = element.split(",");

	        for (String sNum : stringNumbers) {
	            Integer num = Integer.valueOf(sNum);
	            list2.add(num);
	        }
	    };
//	    BiConsumer<List<Integer>, List<Integer>> combiner = (listA , listB) -> listA.addAll(listB);
		List<Integer> list = Arrays.stream(arr).collect(ArrayList::new, accumulator, List::addAll);
		
	    System.out.println(list);
	}
}
