package aufgaben.streams.collect.warenkorb;

import java.util.HashMap;
import java.util.Map;

public class Preise {

	public static Map<String, Integer> STANDERD_PREISE = new HashMap<>();
	
	static {
		STANDERD_PREISE.put("Brot", 129);
		STANDERD_PREISE.put("Wurst", 200);
		STANDERD_PREISE.put("Milch", 100);
	}
	
}
