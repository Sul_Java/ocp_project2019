package aufgaben.streams;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.stream.Stream;

public class KleineAufgabeCollect {
	
	static class Kreis implements Comparable<Kreis>{
		int radius;

		public Kreis(int radius) {
			this.radius = radius;
		}
		@Override
		public String toString() {
			return "Kreis R= " + radius;
		}
		@Override
		public int compareTo(Kreis k) {
			return radius - k.radius;
		}
		
		
	}
	public static void main(String[] args) {
		
		a1();
		a2();
		
	}

	private static void a2() {
		System.out.println("A2*********");
		
		Integer[] radien = { 2 , 4 , 1 , 5 };
		
		
		TreeSet<Kreis> result = Arrays.stream(radien)
									.map(Kreis::new)
									.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);
		
		System.out.println(result);
	}

	private static void a1() {
		System.out.println("A1*********");
		StringBuilder result = Stream.of("a" , "bb" , "ccc")
							.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);
		
		System.out.println(result); // abbccc
		
	}

}
