package aufgaben.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

public class AufgabeStream_count_min_max {
	
	public static void main(String[] args) {
		
		a1();
		a2();
		a3();
		
	}

	private static void a3() {
		System.out.println("******** A3");
		
		Locale[] locales = Locale.getAvailableLocales();
		
		// A    
		List<Locale> filtered = new ArrayList<>();
		for (Locale locale : locales) {
		    if(locale.getDisplayCountry().contains("t")) {
		        filtered.add(locale);
		    }
		}

		Comparator<Locale> cmp = (loc1, loc2) -> 
		        loc1.getDisplayLanguage().compareTo(loc2.getDisplayLanguage());

		filtered.sort( cmp );

		if(filtered.size() > 0) {
		    Locale min = filtered.get( 0 );
		    System.out.println( min.getDisplayCountry() );
		    System.out.println( min.getDisplayLanguage() );
		}
		// B
		
		System.out.println("**** mit Pipeline ****");
		
		Optional<Locale> min = Arrays.stream(locales)
			.filter( contry -> contry.getDisplayCountry().contains("t"))
//			.sorted(Comparator.comparing(Locale::getDisplayLanguage))
			.min(Comparator.comparing(Locale::getDisplayLanguage));
		System.out.println(min.get().getDisplayCountry() + "\n" 
							+ min.get().getDisplayLanguage());
		// B
		/*
		 * 	- nur die Elemente ber�cksichtigt, die in dem Display-Country ein 't' 
		 *  haben (in dem Wert aus der Methode `getDisplayCountry`)
		 * 
		 * - diese Elemente aufsteigend nach dem Display-Language sortiert 
		 * 	(lexikographisch nach dem Wert aus `getDisplayLanguage`)
		 * 
		 * - Display-Country und Display-Language f�r das kleinste Element 
		 * 	(falls vorhanden) ausgibt
		 */
		
	}

	private static void a2() {
		System.out.println("******** A2");
		
		Locale[] locales = Locale.getAvailableLocales();
		
		
		long anzahl = Stream.of(locales)
			.filter( l -> l.getLanguage() == "de")
			.count();
		System.out.println("de Anzahl : " + anzahl );
		
	}

	private static void a1() {
		System.out.println("******* A1");
		
		Locale[] locales = Locale.getAvailableLocales();
		
		Comparator<Locale> comparator = 
//				(l1 , l2) -> l1.getDisplayCountry().compareTo(l2.getDisplayCountry());
				Comparator.comparing(Locale::getDisplayCountry);
		Optional<Locale> max = Arrays.stream(locales)
									.max(comparator);
		System.out.println(max.get());
		
	}
	
}
