package aufgaben.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class AufgabeStreamBilden {
	
	static class Path{
		String text;
		public static Path get(String s) {
			return new Path(s);
		}
		public Path(String text) {
			this.text = text;
		}
		@Override
		public String toString() {
			return text;
		}
		
	}
	
	static class Test {
	    static Integer nextInt() {
	        return new Random().nextInt();
	    }
	}
	
	public static void main(String[] args) {
//		a1();
//		a2();
//		a3();
//		a4();
		a5();
		
	}
	
	static void a5() {
		//A
	    Collection<Path> coll = new ArrayList<>();
	    coll.add(Path.get("/a"));
	    coll.add(Path.get("/a/b"));
	    coll.add(Path.get("/a/b/c"));
	    coll.add(Path.get("/a/b/c/d"));
	    for(Path p : coll) {
	        System.out.println(p);
	    }
	    System.out.println("***** mit stream *****");
	    
	    Consumer<Collection<Path>> c = e -> {
	    	for (Path path : coll) {
				System.out.println(path);
			}
	    };
		Stream.of(coll)
	    	.forEach(c );
	    
	}
	
	static void a4() {
		
		String[] a1 = { "a", "b" };
	    String[] a2 = { "c", "d" };

	    // A
	    String[][] a3 = { a1, a2 };
	    for (String[] arr : a3) {
	        for (String s : arr) {
	            System.out.println(s);
	        }
	    }
	    System.out.println("mit Stream");
	    
	    Stream.concat(Arrays.stream(a1), Arrays.stream(a2))
	    	  .forEach(System.out::println);
	}
	
	static void a3() {
		
		for (int i = 100; i >= 1; i--) {
	        System.out.println( i );
	    }
		
		Stream.iterate(100, x -> x-1)
			  .limit(100)
		   	  .forEach(System.out::println);;
	}
	
	static void a2() {
		for (int i = 1; i < 100; i++) {
			System.out.println( Test.nextInt() );
			}
		System.out.println("mit stream");
		
		Stream.generate( Test::nextInt)
			  .limit(100)
			  .forEach(System.out::println);;
	}
	
	static void a1() {
		
		List<Integer> list1 = Arrays.asList( 1, 2, 3 );
	    List<Integer> list2 = Arrays.asList( 55, 77 );

	    // A
	    List<List<Integer>> list3 = Arrays.asList(list1, list2);
	    for(List<Integer> e : list3) {
	        System.out.println("size = " + e.size() + ". elements = " + e);
	    }

	    System.out.println("****** A1");
		Stream.of(list1 , list2)
			  .forEach(list -> System.out.println("size = " + list.size() + ". elements = " + list));

	}

}
