package aufgaben.streams.words;

import java.util.List;
import java.util.Optional;


public class AufgabeWords_funkt_streams {
	
	static List<String> listWoerter = ResourceLoader.loadEnglishWords();
	
	public static void main(String[] args) {
		
//		a1();
		a2();
		
	}
	
	private static void a3() {
		System.out.println("******** A3");
		
//		listWoerter.stream().
	}

	private static void a2() {
		System.out.println("******** A2");
		
		Optional<String> minWort = listWoerter.stream().filter(str -> str.charAt(0) == 'm')
													.min((s1 ,s2) -> s1.compareTo(s2) );
		System.out.println(minWort.get());
		
	}

	private static void a1() {
		System.out.println("******** A1");
		
		listWoerter.stream().filter(str -> str.length() > 5)
		.forEach(System.out::println);;
		
	}

}
