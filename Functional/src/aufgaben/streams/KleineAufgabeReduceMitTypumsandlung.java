package aufgaben.streams;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;


public class KleineAufgabeReduceMitTypumsandlung {
	static class Rechteck {
		private int breite,hoehe;

		public Rechteck(int breite, int hoehe) {
			this.breite = breite;
			this.hoehe = hoehe;
		}
		
		int getFlaeche(){
			return breite * hoehe;
		}
	}

	static class ValueHolder{
		int vlaue;
	}
	
	public static void main(String[] args) {
		
		Rechteck[] arr = {
			new Rechteck(5, 10),
			new Rechteck(10, 20),
			new Rechteck(20, 30),
			new Rechteck(30, 40)
		};
		
		ValueHolder holder = new ValueHolder();
		
		Arrays.stream(arr).map(Rechteck::getFlaeche).forEach(flaeche ->{
			holder.vlaue += flaeche;
		});;
		System.out.println(holder.vlaue);
		
		// A3 Besser
		
		Integer identity = 0;
		BinaryOperator<Integer> combiner = (f1 ,f2) -> f1 + f2;
		BiFunction<Integer, Rechteck , Integer> accum = (f , r) -> f + r.getFlaeche();
		Integer gesamt = Arrays.stream(arr).reduce(identity , accum, combiner);
		System.out.println(gesamt);
		
//		Rechteck identity = new Rechteck(0, 0);
//		
//		BinaryOperator<Rechteck> op = new BinaryOperator<Rechteck>() {
//			@Override
//			public Rechteck apply(Rechteck r1, Rechteck r2) {
//				int sum = r1.getFlaeche() + r2.getFlaeche();
//				return ;
//			}
//		};
//		Arrays.stream(rechtecken)
//			.reduce(identity ,op ,op);
	}
}
