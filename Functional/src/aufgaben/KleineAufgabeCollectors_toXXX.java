package aufgaben;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KleineAufgabeCollectors_toXXX {
	
	static class Wochentag{
		String name;
		int nummer;
		public Wochentag(String name, int nummer) {
			this.name = name;
			this.nummer = nummer;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return name + " " + nummer;
		}
	}
	
	public static void main(String[] args) {
		
		Integer[] zahlen = {1 , 2 , 3 , 4 , 5 , 6 ,7};
		String[] namen = {"mo" , "di" , "mi" , "do" , "fr" , "sa" , "so"};
		
		
		Collector<String[] , ? , List<Wochentag>> coll;
		
		Supplier<Wochentag> supplier = null;
		BiConsumer<Wochentag , List<Wochentag>> accumulator = null;
		BiConsumer<List<Wochentag> , List<Wochentag>> combiner;
		
		LinkedList<Wochentag> list = Stream.iterate(0 ,  i -> i+1)
										.limit(7)
										.map(i -> new Wochentag(namen[i], zahlen[i]))
										.collect(Collectors.toCollection(LinkedList::new));
		
		list.forEach(System.out::println);
		
	}

}
