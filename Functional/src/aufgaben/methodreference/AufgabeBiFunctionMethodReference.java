package aufgaben.methodreference;

import java.util.function.BiFunction;

class Auto{
	@Override
	public String toString() {
		return "Auto" + hashCode();
	}
	
	Besitzer build(Integer id) {
		return new Besitzer(this, id);
	}
}

class Besitzer{
	private Auto a;
	private Integer id;
	public Besitzer(Auto a, Integer id) {
		this.a = a;
		this.id = id;
	}
	
	static Besitzer build(Auto a , Integer id) {
		return new Besitzer(a, id);
	}
	@Override
	public String toString() {
		return "Besitzer"  + id + ", " + a;
	}
}

class BesitzerBuilder{
	public Besitzer builder(Auto a , Integer id) {
		return new Besitzer(a, id);
	}
}
public class AufgabeBiFunctionMethodReference {

	public static void main(String[] args) {
		
		BiFunction<Auto, Integer, Besitzer> f1 = new BiFunction<>() {
			@Override
			public Besitzer apply(Auto a, Integer id) {
				return new Besitzer(a,id);
			}
		};
		
		Besitzer b1 = f1.apply(new Auto(), 11);
		System.out.println(b1);
		
		BiFunction<Auto, Integer, Besitzer> f2 = (a , id) -> new Besitzer(a, id);
		Besitzer b2 = f2.apply(new Auto(), 12);
		System.out.println(b2);
		
		BiFunction<Auto, Integer, Besitzer> f3 = Besitzer::build;
		Besitzer b3 = f3.apply(new Auto(), 13);
		System.out.println(b3);
		
		BiFunction<Auto, Integer, Besitzer> f4 = Besitzer::new;
		Besitzer b4 = f4.apply(new Auto(), 14);
		System.out.println(b4);
		
		BesitzerBuilder b = new BesitzerBuilder();
		BiFunction<Auto, Integer, Besitzer> f5 = b::builder;
		Besitzer b5 = f5.apply(new Auto(), 15);
		System.out.println(b5);
		
		BiFunction<Auto, Integer, Besitzer> f6 = Auto::build;
		Besitzer b6 = f6.apply(new Auto(), 16);
		System.out.println(b6);
	}
}
