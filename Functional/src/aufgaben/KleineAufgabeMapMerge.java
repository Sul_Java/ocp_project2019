package aufgaben;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class KleineAufgabeMapMerge {
	
	public static void main(String[] args) {
		
		String text = "Heute ist Freitag";
		
		Map<Character, Integer> countMap = new HashMap<>();
		
//		for (int i = 0; i < text.length(); i++) {
//			char ch = text.charAt(i);
//			
//			Integer count = countMap.get(ch);
//			
//			if (count == null ) {
//				count = 0;
//			}
//			
//			countMap.put(ch, ++count);
//		}
		
		for (Character ch : text.toCharArray()) {
			countMap.merge(ch, 1, (ch2 , oldCount) -> oldCount+1);
		}
		
		System.out.println(countMap);
		
		countMap = Stream.of(text.toCharArray()).collect(null);
		
	}

}
