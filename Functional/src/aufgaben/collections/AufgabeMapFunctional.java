package aufgaben.collections;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AufgabeMapFunctional {

	static Map<Integer, String> buildMap(){
		Function<String, Integer> keyMapper = s -> {
			switch (s) {
			case "Mo":return 1;
			case "Di":return 2;
			case "Mi":return 3;
			}
			throw new IllegalArgumentException("Bad argument " + s);
		};
		return Arrays.asList("Mo" ,"Di" , "Mi").stream()
						.collect(Collectors.toMap(keyMapper, s -> s));
	}
	
	public static void main(String[] args) {

		a1();
		a2();
		a3();
		a4();

	}
	
	static void a4() {
		System.out.println("***** A4");
		
		Map<Integer, String> map = buildMap();
		
		map.computeIfPresent(2, (k,v) -> "Dienstag");
		System.out.println(map);
		
	}
	
	static void a3() {
		System.out.println("***** A3");
		
		Map<Integer, String> map = buildMap();
		
		map.computeIfAbsent(2, k -> "Dienstag");
		System.out.println(map);
		
		map.computeIfPresent(2, (k,v) -> "Dienstag");
		System.out.println(map);
		
	}
	
	static void a2() {
		System.out.println("***** A2");
		
		Map<Integer, String> map = buildMap();
		
		map.compute(2, (k,v) -> "Dienstag");
		System.out.println(map);
		
		map.compute(4, (k,v) -> "Dienstag");
		System.out.println(map.get(4));
		
	}
	
	static void a1() {
		System.out.println("***** A1");
		
		Map<Integer, String> map = buildMap();
		
		map.forEach( (k ,v) -> System.out.println(k + " = " + v));
		
	}

}
