package aufgaben.collections;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AufgabeCollectionsFunctional {
	
	static List<Integer> buildListNumber(){		
		return Stream.iterate(1, x -> x+1).limit(8)
							.collect(Collectors.toCollection(ArrayList::new));
	}
	
	public static void main(String[] args) {
		
		a2();
		a3();
		a4();
		a5();
		
	}
	
	static void a5() {
		System.out.println("***** A5");
		
		List<Integer> list = buildListNumber();
		System.out.println(list);
		
		list.replaceAll(x -> x==3? null : x);
		System.out.println(list);
		
		int sum = list.stream().filter(x -> x != null).reduce(0,(a , b) -> a+b);
		System.out.println(sum);
		
	}
	
	static void a4() {
		System.out.println("***** A4");
		
		List<Integer> list = buildListNumber();
		System.out.println(list);
		
//		list.sort(new Comparator<Integer>() {
//			@Override
//			public int compare(Integer i1, Integer i2) {
//				return i2 - i1;
//			}
//		});
		
//		list.sort((a ,b) -> b-a);
		
		Comparator<Integer> cmp = new Comparator<Integer>() {
			@Override
			public int compare(Integer i1, Integer i2) {
				return i2 - i1;
			}
		};
		list.sort(cmp::compare);
		System.out.println(list);
		
	}
	
	static void a3() {
		System.out.println("***** A3");
		
		List<Integer> list = buildListNumber();
		System.out.println(list);
		
		list.replaceAll(x -> x%2 != 0 ? 0 : x);
		System.out.println(list);
		
	}
	static void a2() {
		System.out.println("***** A2");
		
		List<Integer> list = buildListNumber();
		System.out.println(list);
		
		list.removeIf(x -> x%2 != 0);
		System.out.println(list);
	}

}
