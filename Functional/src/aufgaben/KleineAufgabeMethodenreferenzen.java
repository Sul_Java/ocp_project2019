package aufgaben;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class KleineAufgabeMethodenreferenzen {
	
	static class Values{
		Values(){
		}
		Values(Values v) {
		}
		
		static String get(Values v , Integer i) {
			return "";
		}
	}
	
	public static void main(String[] args) {
		
		Supplier<Values> s1 = Values::new;
		
		Consumer<Values> c1 = Values::new;
		
		
		BiFunction<Values, Integer, String> f1 = Values::get;
		
		BiFunction<Values, Integer, String> f2 = new BiFunction<>() {
			@Override
			public String apply(Values t, Integer u) {
				return null;
			}
		};
		
//		Supplier<String> s2 = 
		
	}

}
