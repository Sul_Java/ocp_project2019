package concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorService_ThreadPool {
	
	public static void main(String[] args) {
		
		List<Runnable> listTasks = new ArrayList<>();
		
		for (int i = 0; i < 1; i++) {
			Runnable task = () -> 
			System.out.println( "my task in thread " + Thread.currentThread().getId() );
			listTasks.add(task);
		}
		
		ExecutorService service = Executors.newSingleThreadExecutor();
		
		for (Runnable task : listTasks) {
			service.execute(task);
		}
		
		service.shutdown();
		
	}

}
