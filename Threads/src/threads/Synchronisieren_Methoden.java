package threads;

class IncrementValue{
	private int value;
	
//	public void increment() {
//		synchronized (this) {
//			value++;
//		}
//	}
	
	public synchronized void increment() {
		value++;
	}
	// weitere Methoden
}

class IncrementStaticValue{
	private static int value;
	
	public synchronized static void increment() {
		synchronized (IncrementStaticValue.class) {
			value++;
		}
	}
	// weitere Methoden
}

public class Synchronisieren_Methoden {

	public static void main(String[] args) {
		
		
		
	}
}
