package threads;

public class Thread_API_einfach {
	
	public static void main(String[] args) {
		
		new Thread();
		
		Runnable target = () -> {};
		new Thread(target);
		
		String name = "Tom";
		new Thread(name);
		
		new Thread(target, name);
		
		Thread t1 = new Thread();
		System.out.println(t1);
		
		System.out.println(t1.getId());
	}

}
