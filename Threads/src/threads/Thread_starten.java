package threads;

public class Thread_starten {
	
	public static void main(String[] args) {
		
//		Thread thread = new Thread();
//		thread.start();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("main");
			}
		}) {
			@Override
			public void run() {
				System.out.println("run");
			}
		}.start();
		
	}

}
