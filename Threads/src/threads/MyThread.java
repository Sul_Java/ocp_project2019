package threads;

public class MyThread extends Thread{
	
	@Override
	public void run() {
		System.out.println("run");
	}
	
	public static void main(String[] args) {
		
		Thread th = new MyThread();
		th.start();
		System.out.println("main");
		
	}

}
