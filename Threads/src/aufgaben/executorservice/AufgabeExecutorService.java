package aufgaben.executorservice;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class AufgabeExecutorService {
	
	public static void main(String[] args) {
		
		List<String> englishWordsList = ResourceLoader.loadFromZipResource("english_words_lowercase.zip");

//		a2(englishWordsList);
//		a3(englishWordsList);
		
//		a4(englishWordsList);
		
//		a5(englishWordsList);
		
		a6(englishWordsList);
		
	}

	private static void a6(List<String> englishWordsList) {
		System.out.println("**** A6");
		
		final int countTasks = 50;
		final int subListSize = englishWordsList.size()/countTasks;
		
		for (int i = 0; i < countTasks; i++) {
			
		}
		
	}

	private static void a5(List<String> englishWordsList) {
		System.out.println("**** A5");
		
		Callable<Long> taskCount = () -> englishWordsList.stream()
												.filter(str -> str.length() == 5)
												.count();
		
		ExecutorService service = Executors.newSingleThreadExecutor();
		
		try {
			long count = service.submit(taskCount).get();
			System.out.println(count);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		
		service.shutdown();
		
	}

	private static void a4(List<String> englishWordsList) {
		System.out.println("**** A4");
		
		ExecutorService service = Executors.newCachedThreadPool();
		
		AtomicInteger result = new AtomicInteger();
		Runnable task1 = () -> {
			long count = englishWordsList.stream()
									.filter(str -> str.contains("t"))
									.count();
			result.set((int) count);
		};
		
		Future<?> future1 = service.submit(task1);
		
		try {
			future1.get();
			System.out.println("result: " + result);
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void a3(List<String> englishWordsList) {
		System.out.println("**** A3");
		
		Callable<Integer> task = () -> {
			long count = englishWordsList.stream()
					.filter(str -> str.contains("t"))
					.count();
			return (int) count;
		};
	}

	private static void a2(List<String> englishWordsList) {
		System.out.println("**** A2");
		
		Runnable task = () -> {
			long count = englishWordsList.stream()
									.filter(str -> str.contains("t"))
									.count();
			System.out.println(count);
		};
		
	}
}
