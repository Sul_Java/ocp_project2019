package aufgaben;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class KleineAufgabeFuture {

	public static void main(String[] args) throws Exception {
		
		ExecutorService service = Executors.newCachedThreadPool();
		
		Random random = new Random();
		
		List<Future<Integer>> futurs = new ArrayList<>();
		
		for (int i = 0; i < 100; i++) {
			Callable<Integer> task = () -> random.nextInt(6);
			Future<Integer> futur = service.submit(task);
			futurs.add(futur);
		}
		
		int sum = 0;
		for (Future<Integer> future : futurs) {
			sum += future.get();
		}
		
		System.out.println(sum);
	}
}
