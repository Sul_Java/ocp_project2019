package aufgaben;

import java.util.Random;

public class KleineAufgabePriority {

	public static void main(String[] args) {
		
		Runnable target = () -> {
			Random r = new Random();
			long sum = 0;
			for (int i = 0; i < 10_000_00; i++) {
				sum += r.nextInt();
			}
			Thread current = Thread.currentThread();
			System.out.println(current.getName() + ": " + sum);
		};
		
		Thread t1 = new Thread(target , "Tom");
		Thread t2 = new Thread(target , "Jerry");
		
		t1.setPriority(Thread.MIN_PRIORITY);
		t2.setPriority(Thread.MAX_PRIORITY);
		
		t1.start();
		t2.start();
		
	}

}
