package aufgaben;

public class KleineAufgabeJoin {
	
	volatile static int value;
	
	public static void main(String[] args) {
		
		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < 1_000_000; i++) {
					value++;
				}
			}
		};
		t1.start();
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		System.out.println(value);
		System.out.println(value);
		
	}

}
