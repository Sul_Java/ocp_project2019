package aufgaben.logger;

class MyService extends Thread{
	private MyLogger logger = new MyLogger();
	
	public MyService(MyLogger logger) {
		this.logger = logger;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 100; i++) {
			logger.addMessage(getName(), "Message " + i);
		}
	}
}

public class AufgabeThreadsLogger {
	
	public static void main(String[] args) throws InterruptedException {
		
		a1();
		
	}
	
	static void a1() throws InterruptedException {
		
		MyLogger logger = new MyLogger();
		
		Thread t1 = new MyService(logger);
		t1.start();
		Thread t2 = new MyService(logger);
		t2.start();
		
		new MyService(new MyLogger()).start();
		
		t1.join();
		t2.join();
		
		System.out.println("main");
		
		System.out.println(logger.getLog());
	}

}
