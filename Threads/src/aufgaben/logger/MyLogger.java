package aufgaben.logger;

public class MyLogger {

	private final StringBuilder sb = new StringBuilder();

	public void addMessage(String caller, String message) {
		 synchronized (this) {
			 sb.append(caller)
		        .append(" - ")
		        .append(message)
		        .append("\n");
		}
    }

	public String getLog() {
//		synchronized (this) {
//			return sb.toString();
//		}
		return sb.toString();
 	}
	
}