package aufgaben;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

@SuppressWarnings("serial")
class PrintBound extends RecursiveAction{
	private static final int MAX_SIZE = 3;
	
	private int lowerBound , upperBound;

	public PrintBound(int lowerBound, int upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	@Override
	protected void compute() {
		if (upperBound - lowerBound < MAX_SIZE) {
			System.out.println("Unterbereich " + lowerBound + " .. " + upperBound);
		}else {
			
			int center = (upperBound + lowerBound) / 2 ;
			
			invokeAll( new PrintBound(lowerBound, center),
					   new PrintBound(center+1, upperBound)
					);
		}
	}

}

public class KleineAufgabeForkJoin {
	
	public static void main(String[] args) {
		
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(new PrintBound(1,9));
		
	}

}
