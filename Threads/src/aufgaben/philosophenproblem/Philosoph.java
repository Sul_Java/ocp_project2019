package aufgaben.philosophenproblem;

class Philosoph implements Runnable{
	private String name;
	private Object linkeGabel, rechteGabel;
	
	public Philosoph(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true) {
			System.out.println(name + " denkt nach.." );
			pause(10);
			
			System.out.println(name + " hat Hunger");
			
			synchronized (linkeGabel) {
				System.out.println(name + " nimmt die linke Gabel");
				
				pause(100);
			
				synchronized (rechteGabel) {
					System.out.println(name + " nimmt die rechte Gabel");
					System.out.println(name + " isst...");
					
					pause(5000);
					
					System.out.println(name + " legt die rechte Gabel ab");
				}
				System.out.println(name + " legt die linke Gabel ab");
			}
		}
	}

	public void setLinkeGabel(Object linkeGabel) {
		this.linkeGabel = linkeGabel;
	}

	public void setRechteGabel(Object rechteGabel) {
		this.rechteGabel = rechteGabel;
	}
	
	public static void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}