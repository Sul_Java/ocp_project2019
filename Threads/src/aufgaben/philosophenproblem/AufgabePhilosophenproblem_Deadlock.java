package aufgaben.philosophenproblem;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AufgabePhilosophenproblem_Deadlock {

	public static void main(String[] args) {
		
		List<Philosoph> philosophen = getPhilosophen(3);
		List<Thread> threads = philosophen.stream()
									.map(ph -> new Thread(ph))
									.collect(Collectors.toList());
		
		threads.stream().forEach(Thread::start);
		
		List<Object> gablen = Stream.generate(Object::new)
									.limit(philosophen.size())
									.collect(Collectors.toList());
		
		for (int i = 0; i < philosophen.size(); i++) {
			Philosoph ph = philosophen.get(i);
			
			Object linkeGabel = gablen.get(i);
			
			int indexRechteGabel = i == philosophen.size()-1 ? 0:i+1;
			Object rechteGabel = gablen.get(indexRechteGabel);
			
			ph.setLinkeGabel(linkeGabel);
			ph.setRechteGabel(rechteGabel);
		}

	}

	static List<Philosoph> getPhilosophen(int i) {
		
		List<Philosoph> list = Arrays.asList(
				new Philosoph("Sokrat"),
				new Philosoph("Arosto"),
				new Philosoph("Seqmond")
				);
		return list;
	}

}
