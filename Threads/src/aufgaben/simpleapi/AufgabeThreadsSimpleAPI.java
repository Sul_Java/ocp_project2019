package aufgaben.simpleapi;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AufgabeThreadsSimpleAPI {
	
	public static void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
//		a1();
//		a2();
//		a3();
		a4();
		
	}
	
	static void a4() {
		class PrintChar implements Runnable{
			char ch;
			
			PrintChar(char ch){
				this.ch = ch;
			}
			public void run() {
				System.out.println(ch);
			}
		}
		
		for (char ch = 'A'; ch <= 'Z'; ch++) {
//			PrintChar target = new PrintChar(ch);
			char letter = ch;
			Runnable target = () -> System.out.println(letter);
			new Thread(target).start();
		}
	}
	
	static void a3() {
		
		Runnable target = () -> System.out.println(Thread.currentThread().getId());
		
		List<Thread> list = Stream.generate(() -> new Thread(target))
									.limit(37)
									.collect(Collectors.toList());
		
//		list.forEach(th -> System.out.println(th.getId()));
		
		list.forEach(Thread::start); // Reihenfolge nicht garantiert
	}

	static void a2() {
		Runnable target = () -> {
			while (true) {
				Thread th = Thread.currentThread();
				System.out.println(th.getName() + ": " + th.getId());
				pause(1_000);
			}
		};
		
		new Thread(target).start();
	}
	
	static void a1() {
		
		new Thread() {
			public void run() {
				while (true) {
					System.out.println(getName() + ": " +getId());
					pause(1_000);
				}
			};
		}.start();;
	}
}
