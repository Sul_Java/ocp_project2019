package aufgaben.concurrency;

import java.math.BigInteger;
import java.util.Arrays;

public class AufgabePermutation {
	
	public static void main(String[] args) {
		
		char[] arr = {
			'a',
			'b',
			'c',
			'd'
		};
		
		BigInteger arrayLen = BigInteger.valueOf(arr.length);
		BigInteger countPerm = factorial(arrayLen);
		
		System.out.printf("Arrays, länge = %d %n" , arr.length);
		System.out.printf("Permutation %s %n" , countPerm);
		
		permutate(arr, arr.length);
		
	}

	static BigInteger factorial(BigInteger bi) {
		if (bi.intValue() == 1) {
			return bi;
		}
		return bi.multiply(factorial(bi.subtract(BigInteger.ONE)));
	}

	static void permutate(char[] arr , int pointer) {
		if (pointer==1) {
			System.out.printf("%s %n" , Arrays.toString(arr));
			return;
		}
		
		for (int i = 0; i < pointer; i++) {
			permutate(arr, pointer-1);
			
			if (pointer%2 == 0) {
				char tmp = arr[pointer-1];
				arr[pointer-1] = arr[i];
				arr[i] = tmp;
			}else {
				char tmp = arr[pointer-1];
				arr[pointer-1] = arr[0];
				arr[0] = tmp;
			}
		}
		permutate(arr, pointer-1);
	}
}
