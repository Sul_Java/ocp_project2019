package aufgaben.concurrency.cyclicbarrier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class RaceCar implements Runnable {
    private String name;
    private List<RaceCar> finish;

    public RaceCar(String name, List<RaceCar> finish) {
        this.name = name;
        this.finish = finish;
    }

    public void run() {
        System.out.println(name + " started.");

        finish.add(this);
        System.out.println(name + " finished.");
    }
    public String toString() {
        return name;
    }
}

public class AufgabeCyclicBarrier {
	
	static int count;
	
	public static void main(String[] args) throws InterruptedException {
		
//		a1();
		
		a2();
		
	}
	
	static void a2() throws InterruptedException {
		
		List<RaceCar> finish = new ArrayList<>();
		
		List<RaceCar> autos = Arrays.asList(
					new RaceCar("VW", finish),
					new RaceCar("Audi", finish),
					new RaceCar("Opel", finish),
					new RaceCar("BMW", finish)
				);
		
		Runnable showResult = () -> {
			System.out.println("Erg . ");
			for (int i = 0; i < finish.size(); i++) {
				System.out.println((i+1) + ". " + finish.get(i));
			}
		};
		
		CyclicBarrier barrier = new CyclicBarrier(autos.size(), showResult);
		
		ExecutorService rennenService = Executors.newFixedThreadPool(autos.size());
		
		autos.forEach(rennenService::submit);
		
		rennenService.shutdown();
		
	}
	
	static void a1() {
		
		int parties = 2;
		
		CyclicBarrier barrier = new CyclicBarrier(parties, () -> System.out.println(count));
		
		Runnable inc = () -> {
			for (int i = 0; i < 1_000_000; i++) {
				synchronized (barrier) {
					count++;
				}
			}
			try {
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		};
		
		ExecutorService service = Executors.newFixedThreadPool(parties);
		
		service.execute(inc);
		service.execute(inc);
		
		service.shutdown();
	}

}
