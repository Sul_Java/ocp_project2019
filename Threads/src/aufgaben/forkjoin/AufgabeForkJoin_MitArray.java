package aufgaben.forkjoin;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

@SuppressWarnings("serial")
class RecursiveReplaceNigative extends RecursiveAction{
	private static final int MAX_ELEMENT = 3;
	
	private int[] array ;
	private int indexFrom , indexTo;
		
	public RecursiveReplaceNigative(int[] array , int indexFrom , int indexTo) {
		this.array = array;
		this.indexFrom = indexFrom;
		this.indexTo = indexTo;
	}

	@Override
	protected void compute() {
		if(indexTo - indexFrom <= MAX_ELEMENT) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] < 0 ) {
					array[i] = 0;
				}
			}
		} else {
			int mitte = (indexFrom + indexTo) / 2 ;
			invokeAll(new RecursiveReplaceNigative(array, indexFrom, mitte),
					new RecursiveReplaceNigative(array, mitte, indexTo) );
		}
	}
	
}

class CountNegative extends RecursiveTask<Integer>{
	private static final int MAX_ELEMENT = 3;
	
	private int[] array ;
	private int indexFrom , indexTo;
	
	public CountNegative(int[] array, int indexFrom, int indexTo) {
		this.array = array;
		this.indexFrom = indexFrom;
		this.indexTo = indexTo;
	}

	@Override
	protected Integer compute() {
		if (indexTo - indexFrom <= MAX_ELEMENT) {
			int count = 0;
			
			for (int i = indexFrom; i < indexTo; i++) {
				if (array[i] < 0) {
					count++;
				}
			}
			return count;
		}else {
			int mitte = (indexFrom + indexTo) / 2;
			
			CountNegative taskA = new CountNegative(array, indexFrom, mitte);
			CountNegative taskB = new CountNegative(array, mitte, indexTo);
			
			taskA.fork();
			
			return taskB.compute() + taskA.join();
		}
	}
	
}

public class AufgabeForkJoin_MitArray {
	
	public static void main(String[] args) {
		
		Random random = new Random();
		
		IntSupplier s = () -> random.nextInt(101) - 50;
		
		int[] array = IntStream.generate(s).limit(10).toArray();
		
//		a1(array);
		a2(array);
		
		
	}
	
	static void a2(int[] array) {
		System.out.println(Arrays.toString(array));
		
		ForkJoinPool pool = new ForkJoinPool();
		
		RecursiveTask<Integer> countNig = new CountNegative(array , 0 , array.length);
		
		Integer count = pool.invoke(countNig);
		System.out.println(count);
		
	}
	
	static void a1(int [] array) {
		
		System.out.println(Arrays.toString(array));
		
		RecursiveAction task1 = new RecursiveReplaceNigative(array , 0 , array.length);
				
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(task1);
		
		System.out.println(Arrays.toString(array));
		
	}

}
