package aufgaben.forkjoin;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public class AufgabeForkJoin_MitList {
	
	static class ToUpperCaseAction extends RecursiveAction{
		private static final int MAX_LIST_SIZE = 1000; 
		
		private List<String> list;

		public ToUpperCaseAction(List<String> list) {
			this.list = list;
		}

		@Override
		protected void compute() {
			if (list.size() < MAX_LIST_SIZE) {
				for (int i = 0; i < list.size(); i++) {
					list.set(i, list.get(i).toUpperCase());
				}
			} else {
				
				int mitte = list.size() / 2;
				List<String> listA = list.subList(0, mitte);
				List<String> listB = list.subList(mitte, list.size());
				
				invokeAll(new ToUpperCaseAction(listA),
						new ToUpperCaseAction(listB));
			}
		}
	}
	
	static class GetMaxTask extends RecursiveTask<String>{
		private final int MAX_LIST_SIZE = 2000;
		
		private List<String> list;
		
		public GetMaxTask(List<String> list) {
			this.list = list;
		}
		
		static String getMax(String s1 , String s2) {
			return s1.compareTo(s2) > 0 ? s1 : s2;
		}
		
		@Override
		protected String compute() {
			if (list.size() <= MAX_LIST_SIZE) {
				String max = " ";
				for (String s : list) {
					max = getMax(max , s);
				}
				return max;
			} else {
				 int mitte = list.size() / 2 ;
				 
				 GetMaxTask t1 = new GetMaxTask(list.subList(0, mitte));
				 GetMaxTask t2 = new GetMaxTask(list.subList(mitte , list.size()));
				 
				 t1.fork();
				 String s2 = t2.compute();
				 String s1 = t1.join();
				 
				 return getMax(s1, s2);
			}
		}
		
	}
	
	public static void main(String[] args) {
		
//		a2();
		a3();
		
	}
	
	static void a3() {
		
		List<String> list = ResourceLoader.loadFromZipResource("10k_most_common_passwords.zip");
		
		ForkJoinPool pool = new ForkJoinPool();
		String max = pool.invoke(new GetMaxTask(list));
		
		System.out.println(max);
	}
	
	static void a2() {
		
		List<String> list = ResourceLoader.loadFromZipResource("10k_most_common_passwords.zip");
		
		RecursiveAction taskToUpperCase = new ToUpperCaseAction(list);
		
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(taskToUpperCase);
		
		list.forEach(System.out::println);
		
	}

}
