package aufgaben;

import java.util.stream.Collectors;
import java.util.stream.Stream;

//class  Printer extends Thread{
//	char ch ;
//	int zahlZeichen , zeilen;
//	public Printer(char ch, int zahlZeichen, int zeilen) {
//		this.ch = ch;
//		this.zeilen = zeilen;
//		this.zahlZeichen = zahlZeichen;
//	}
//	public void run() {
//		
//			for (int i = 0; i < zeilen; i++) {
//				synchronized (System.out) {
//					for (int j = 0; j < zahlZeichen; j++) {
//						System.out.print(ch);
//					}
//				System.out.println();
//			}
//		}
//	}
//}

class Printer extends Thread{
	private int countZeilen;
	private String textZeilen;
	public Printer(char ch , int charProZeile, int countZeilen) {
		this.countZeilen = countZeilen;
		
		textZeilen = Stream.generate(() -> String.valueOf(ch))
								.limit(charProZeile)
								.collect(Collectors.joining());
	}
	
	 	
}

public class KleineAufgabeSynchronisieren {
	
	public static void main(String[] args) {
		
		Printer p1 = new Printer('a' , 10 , 20);
		p1.start();
		
		Printer p2 = new Printer('*' , 17 , 30);
		p2.start();
		
	}

}
