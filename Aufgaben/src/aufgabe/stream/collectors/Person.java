package aufgabe.stream.collectors;

public class Person implements Comparable<Person>{
	private String name;
    private String beruf;

    public Person(String name, String beruf) {
        this.name = name;
        this.beruf = beruf;
    }
    // more methods here...
    public String toString() {
        return name;
    }
	@Override
	public int compareTo(Person p2) {
		int erg = name.compareTo(p2.name);
		if (erg == 0 ) {
			erg = beruf.compareTo(p2.beruf);
		}
		return erg;
	}
	
	public String getName() {
		return name;
	}
	
	public String getBeruf() {
		return beruf;
	}
}
