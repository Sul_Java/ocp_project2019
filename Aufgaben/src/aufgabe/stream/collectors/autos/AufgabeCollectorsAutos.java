package aufgabe.stream.collectors.autos;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AufgabeCollectorsAutos {
	
	public static void main(String[] args) {
		
		List<Auto> autos = Arrays.asList(
                new Auto("VW", "Golf"),
                new Auto("VW", "Polo"),
                new Auto("Opel", "Corsa"),
                new Auto("Opel", "Astra")
            );
		
		a1(autos);
		a2(autos);
		a3(autos);
		a4(autos);
		a5(autos);
		
	}

	private static void a5(List<Auto> autos) {
		System.out.println("***** A5 *****");
		
		Predicate<Auto> predicate = auto -> auto.getModell().contains("o");
		
		Collector<Auto, ?, Map<Boolean, List<Auto>>> collector = Collectors.partitioningBy(predicate);
		
		Map<Boolean, List<Auto>> map = autos.stream().collect(collector);
		System.out.println(map);
		
	}

	private static void a4(List<Auto> autos) {
		System.out.println("***** A4 *****");
		
		Function<Auto, String> classifier = auto -> auto.getHersteller();
		Supplier<Map<String, List<Auto>>> mapFactory = TreeMap::new;
		Collector<Auto, ?, List<Auto>> downstream = Collectors.toList();
		
		Collector<Auto, ?, Map<String, List<Auto>>> collector = Collectors.groupingBy(classifier,mapFactory ,downstream);
		
		Map<String, List<Auto>> map = autos.stream().collect(collector);
		System.out.println(map); 
		// Ausgabe: {Opel=[Opel/Corsa, Opel/Astra], VW=[VW/Golf, VW/Polo]}
		
	}

	private static void a3(List<Auto> autos) {
		System.out.println("***** A3 *****");
		
		Function<Auto, String> mapper = Auto::getModell;
		Collector<String, ?, List<String>> downstream2 = Collectors.toList();
		Collector<Auto, ?, List<String>> downstream = Collectors.mapping(mapper, downstream2);
		
		Function<Auto, String> classifier = Auto::getHersteller;
		
		Collector<Auto, ?, Map<String, List<String>>> collector = Collectors.groupingBy(classifier, downstream);
		
		Map<String, List<String>> map = autos.stream().collect(collector );
		System.out.println(map); 
		// mögliche Ausgabe: {VW=[Golf, Polo], Opel=[Corsa, Astra]}
	}

	private static void a2(List<Auto> autos) {
		System.out.println("***** A2 *****");	
		
//		Function<Auto, String> classifier = auto -> auto.getHersteller();
		Collector<Auto, ?, Map<String, List<Auto>>> collector = Collectors.groupingBy(Auto::getHersteller);
		
		Map<String, List<Auto>> map = autos.stream().collect(collector);
		System.out.println(map); 
		// mögliche Ausgabe: {VW=[VW/Golf, VW/Polo], Opel=[Opel/Corsa, Opel/Astra]}
	}

	private static void a1(List<Auto> autos) {
		System.out.println("***** A1 *****");
		
//		Function<Auto, String> mapper = auto -> auto.getHersteller();
//		Collector<String, ?, Set<String>> downstream = Collectors.toSet();
		
		Collector<Auto, ?, Set<String>> collector = Collectors.mapping(Auto::getHersteller , Collectors.toSet());
		
		Set<String> set = autos.stream().collect(collector);
	    System.out.println(set); // mögliche Ausgabe: [VW, Opel]
	}

}
