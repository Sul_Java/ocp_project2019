package aufgabe.stream.collectors;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AufgabeCollectorsPersonenCollect {
	static Person[] personen = {
		    new Person("Tom", "Bauarbeiter(in)"),   
		    new Person("Jerry", "Lehrer(in)"),  
		    new Person("Peter", "Metzger(in)"), 
		    new Person("Paul", "Bauarbeiter(in)"),  
		    new Person("Mary", "Lehrer(in)"),   
		};
	
	public static void main(String[] args) {
		
		a1();
		a2();
		a3();
		a4();
		a5();
		
	}
	private static void a5() {
		System.out.println("****** A5 ******");
		
		Predicate<Person> predicate = p -> p.getBeruf() == "Bauarbeiter(in)";
		Collector<Person, ?, List<Person>> downstream = Collectors.toList();
		
		Collector<Person, ?, Map<Boolean, List<Person>>> c =Collectors.partitioningBy(predicate, downstream );
		 
		Map<Boolean, List<Person>> map = Arrays.stream(personen).collect(c);
		System.out.println(map);
		
	}
	private static void a4() {
		System.out.println("****** A4 ******");
		
		Function<Person, String> classifier = p -> p.getBeruf();
		Collector<Person, ?, Map<String, List<Person>>> collector = Collectors.groupingBy(classifier);
		Map<String, List<Person>> map3 = Arrays.stream(personen).collect(collector);
		System.out.println(map3);
		
	}
	private static void a3() {
		System.out.println("****** A3 ******");
		
		Function<Person, String> mapper = Person::getBeruf;
		Collector<String, ?, Set<String>> downstream = Collectors.toSet();
		
		Collector<Person,?, Set<String>> collector = Collectors.mapping(mapper, downstream);
		Set<String> set = Arrays.stream(personen)
								.collect(collector );
		set.forEach(System.out::println);
	}
	private static void a2() {
		System.out.println("****** A2 ******");
		
		Collector<Person, ?, Map<String, Person>> collector = Collectors.toMap(p -> p.getName(), p -> p);
		
		Map<String, Person> map = Arrays.stream(personen).collect(collector);
		map.forEach((name ,person) -> System.out.println(name + "->" + person));
		
	}
	private static void a1() {
		System.out.println("****** A1 ******");
		
		Collector<Person, ?, TreeSet<Person>> collector = Collectors.toCollection(TreeSet::new);
		
		TreeSet<Person> set = Arrays.stream(personen).collect(collector);
		set.forEach(System.out::println);
	}

}
