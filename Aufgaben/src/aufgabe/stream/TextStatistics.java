package aufgabe.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;


public class TextStatistics {
    private final String text;

    public TextStatistics(String text) {
        this.text = text;
    }

    public int getCountChars() {
        return text.length();
    }
    public int getCountSpecialChars() {
    	
//    	int count = 0;
//    	for (char ch : text.toCharArray()) {
//			char ch = text.charAt(i);
//			if (!Character.isLetterOrDigit(ch)) {
//				count++;
//			}
//		}
        return (int)text.chars()
        			.filter((int ch) -> !Character.isLetterOrDigit(ch))
        			.count();
    }
    public int getCountLetters() {
    	return (int)text.chars()
     			.filter(Character::isLetter)
     			.count();
    }
    public Optional<String> getMaxWord() {
        return Arrays.stream(text.split(" ")).max(Comparator.naturalOrder());
    }

	public Optional<String> getLongestWord() {
		return Arrays.stream(text.split(" ")).max(Comparator.comparingInt(String::length));
	}
}