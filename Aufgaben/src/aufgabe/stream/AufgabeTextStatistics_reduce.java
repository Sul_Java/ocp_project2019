package aufgabe.stream;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class AufgabeTextStatistics_reduce {
	
	private static class CombinedTextStatictics extends TextStatistics{
		private TextStatistics staticticsA,staticticsB;
		
		public CombinedTextStatictics(TextStatistics staticticsA, TextStatistics staticticsB) {
			super("");
			this.staticticsA = staticticsA;
			this.staticticsB = staticticsB;
		}

		@Override
		public int getCountChars() {
			return staticticsA.getCountChars() + staticticsB.getCountChars();
		}

		@Override
		public int getCountSpecialChars() {
			return staticticsA.getCountSpecialChars() + staticticsB.getCountSpecialChars();
		}

		@Override
		public int getCountLetters() {
			return staticticsA.getCountLetters() + staticticsB.getCountLetters();
		}

		@Override
		public Optional<String> getMaxWord() {
			String str1 = staticticsA.getMaxWord().get();
			String str2 = staticticsB.getMaxWord().get();
			
			return str1.compareTo(str2) > 0 ? Optional.of(str1) : Optional.of(str2);
		}

		@Override
		public Optional<String> getLongestWord() {
			String str1 = staticticsA.getMaxWord().get();
			String str2 = staticticsB.getMaxWord().get();
			
			return str1.length()  > str2.length() ? Optional.of(str1) : Optional.of(str2);
		}
		
		
	}
	
	public static void main(String[] args) {
		
		a2();
		
	}
	
	static void a1() {
		TextStatistics stats = new TextStatistics("Hello Welt !");
		
		System.out.println(stats.getCountChars()); // 12
		System.out.println(stats.getCountSpecialChars()); // 3
		System.out.println(stats.getCountLetters()); // 9
		System.out.println(stats.getMaxWord().get()); // Welt
		System.out.println(stats.getLongestWord().get()); // Hallo
	}
	
	static void a2() {
		TextStatistics stats = new TextStatistics("Hello Welt !");
		
		String[] input = {
			"3 Welt",
			"1 2 Hallo"
		};
		
		TextStatistics identity = new TextStatistics("");
		BiFunction<TextStatistics,String, TextStatistics> accumulator= (statictics , str) ->{
			TextStatistics newStatictics = new TextStatistics(str);
			return new CombinedTextStatictics(statictics , newStatictics);
		};
		BinaryOperator<TextStatistics> combiner = CombinedTextStatictics::new;
		
		stats = Arrays.stream(input).reduce(identity, accumulator, combiner);
		
		System.out.println(stats.getCountChars());// 15
		System.out.println(stats.getCountSpecialChars()); // 3
		System.out.println(stats.getCountLetters()); // 9
		System.out.println(stats.getMaxWord().get()); // Welt
		System.out.println(stats.getLongestWord().get()); // Hallo
	}

}
