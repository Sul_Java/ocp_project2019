package aufgabe.stream;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class AufgabeStream_reduce_Simple {
	
	public static void main(String[] args) {
		a1();
		a2();
	}
	static void a1() {
		 String[] items = { "aa", "bbb", "cccc", "ddddd" };
		 
		 BinaryOperator<Integer> accumulator = (s1,s2) -> s1 + s2;
		 Integer count = Arrays.stream(items)
				 				.map(String::length)
				 				.reduce(0, accumulator );
		 System.out.println(count);
		
	}
	static void a2() {
		String[] items = { "aa", "bbb", "cccc", "ddddd" };
		
		
		
		BinaryOperator<Integer> combiner = (s1,s2) -> s1 + s2;
		BiFunction<Integer , String , Integer> accumulator = (count , str) -> count + str.length();
		
		Integer count = Arrays.stream(items)
 				.reduce(0, accumulator , combiner );
		
		System.out.println(count);
		
	}

}
