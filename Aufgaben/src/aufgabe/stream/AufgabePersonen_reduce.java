package aufgabe.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class AufgabePersonen_reduce {
	
	static class Person{
		String vorname,nachname;

		public Person(String vorname, String nachname) {
			this.vorname = vorname;
			this.nachname = nachname;
		}
		
		static String getMax(String s1 ,String s2) {
			if (s1.compareTo(s2) > 0) {
				return s1;
			}
			return s2;
		}
		
		@Override
		public String toString() {
			return vorname + " " + nachname;
		}
		
		static Person combineMax(Person p1 , Person p2) {
			String vorname = getMax(p1.vorname, p2.vorname);
			String nachname = getMax(p1.nachname, p2.nachname);
			
			return new Person(vorname, nachname);
		}
	}
	
	public static void main(String[] args) {
		/*
		 * Tom Katze
		 * Jerry Maus
		 * Alexander Poe
		 */
		List<Person> personen = Arrays.asList(
				new Person("Tom", "Katze"),
				new Person("Jerry", "Maus"),
				new Person("Alexander", "Poe")
				);
		// V1
		
		BinaryOperator<Person> accumulator = Person::combineMax ;
		Person max1 = personen.stream().reduce(accumulator ).get();
		System.out.println(max1);
		
		// V2
		
		Person identity = new Person("", "");
		Person max2 = personen.stream().reduce(identity , accumulator);
		System.out.println(max2);
		
		// V3
		BiFunction<Person, Person, Person> accumulator2 = accumulator;
		BinaryOperator<Person> combiner = accumulator;
		Person max3 = personen.parallelStream().reduce(identity , accumulator2  , combiner);
		System.out.println(max3);
		
		/*
		 * immutable reduction
		 */
		identity = new Person("", "");
		BinaryOperator<Person> badAccumulator = (p1 , p2) -> {
			p1.vorname = Person.getMax(p1.vorname , p2.vorname);
			p1.nachname = Person.getMax(p1.nachname , p2.nachname);
			return p1;
		};
		
		personen.stream().reduce(identity, badAccumulator);
	}

}
