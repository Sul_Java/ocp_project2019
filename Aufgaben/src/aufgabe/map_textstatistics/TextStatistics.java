package aufgabe.map_textstatistics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class TextStatistics {
	
	private String text;

	private TextStatistics(String text) {
		this.text = text ;
	}

	public static TextStatistics of(String text) {
		return new TextStatistics(text);
	}

	public Collection<Character> getUniqueChars() {
		Collection<Character> coll = new ArrayList<>();
		for (int i = 0; i < text.length(); i++) {
			coll.add(text.charAt(i));
		}
		return coll;
	}
	
	public Map<Character, Integer> getCharCounts() {
		Map<Character, Integer> counts = new TreeMap<>();
		for (int i = 0; i < text.length(); i++) {
			
			char ch = text.charAt(i);
			Integer cnt = counts.get(ch);
			
			if (cnt == null) {
				cnt = 0;
			}
			
			cnt++;
			counts.put(ch, cnt);
		}
		return counts;
		
	}

}
