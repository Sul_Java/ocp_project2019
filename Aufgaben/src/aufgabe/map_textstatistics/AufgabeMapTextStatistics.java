package aufgabe.map_textstatistics;

import java.util.Collection;
import java.util.Map;

public class AufgabeMapTextStatistics {
	
	public static void main(String[] args) {
		
		TextStatistics stat = TextStatistics.of("Heute ist Montag!");
		
		Collection<Character> chars = stat.getUniqueChars();
		
		System.out.println(chars);
		
		Map<Character, Integer> count = stat.getCharCounts();
		
		System.out.println(count);
		
	}

}
