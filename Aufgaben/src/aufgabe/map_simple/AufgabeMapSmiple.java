package aufgabe.map_simple;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class AufgabeMapSmiple {
	
	public static void main(String[] args) {
		
		Map<String, LocalDate> mapDate = new HashMap<>();
		
		mapDate.put("heute", LocalDate.now());
		mapDate.put("gestern", LocalDate.now().minusDays(1));
		mapDate.put("morgen", LocalDate.now().plusDays(1));
		
		Set<String> allK = mapDate.keySet();
		
		
		for (String key : allK) {
			LocalDate value = mapDate.get(key);
			System.out.println(key + "=" + value);
		}
		
		System.out.println("*** entrySet()");
		
		// Map.Entry<String, Integer> entry;
		Set<Map.Entry<String, LocalDate>> allEntries = mapDate.entrySet();
		
		for(Map.Entry<String, LocalDate> en : allEntries) {
			System.out.println(en);
			System.out.println("einzeln: " + en.getKey() + " = " + en.getValue());
		}
		
	}

}
