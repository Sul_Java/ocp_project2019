package aufgabe_deque_mirror;

import java.util.ArrayDeque;
import java.util.Deque;

public class Mirror {
	
	private Deque<Character> deque = new ArrayDeque<>();
	
	public Mirror() {
		deque.add('|');
	}
	
	public void add(char ch) {
		deque.addFirst(ch);
		deque.addLast(ch);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (char c : deque) {
			sb.append(c);
		}
		
		return sb.toString();
	}

	public boolean isEmpty() {
		return deque.size() == 1;
	}

	public void remove() {
		deque.removeFirst();
		deque.removeLast();
	}
}
