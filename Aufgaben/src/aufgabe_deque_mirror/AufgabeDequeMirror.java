package aufgabe_deque_mirror;

public class AufgabeDequeMirror {
	
	 public static void main(String[] args) {
		 
	        Mirror m = new Mirror();

	        for (char ch = 'a'; ch < 'g'; ch++) {
	            m.add(ch);
	            System.out.println(m);
	        }
	        
	        System.out.println("***************************");
	        
	        while( !m.isEmpty() ) {
	            System.out.println(m);
	            m.remove();
	        }
	        
	    }
}
