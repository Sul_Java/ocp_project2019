package aufgabe_interfaces_string_transform;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;


//interface StringTransform{
//	String apply(String s);
//}

public class Aufgabe_Interface_String_Transform {

	public static void main(String[] args) {
		
		String[] arr = { "mo", "di", "mi" };

		List<String> list = transform(arr , s -> s.toUpperCase() );
		System.out.println(list); // [MO, DI, MI]
		 
		list = transform(arr , s -> s+"." );
		System.out.println(list);
		 
		list = transform(arr , s -> "(" + s + ")");
		System.out.println(list);
		 
	}
	

static List<String> transform(String[] values , UnaryOperator<String> logic){
		
		List<String> list = new ArrayList<>();
		for (String e : values) {
			e =  logic.apply(e);
			list.add(e);
		}
		return list;
	}

}
