package aufgabe_gebaeude;

public class Gebaeude {
	
	public class Stockwerk{
		
		public class Raum{
			
			private int nummer;
			
			public Raum(int nummer) {
				this.nummer = nummer;
			}
			
			@Override
			public String toString() {
				return String.format("Raum %d.%d / %s %s",Stockwerk.this.nummer,
															this.nummer ,
															strasse ,
															Gebaeude.this.nummer);
			}
			
		} // End of Raum

		private int nummer;
		private Raum[] raeume;
		
		public Stockwerk(int nummer , int anzahlRaeume) {
			this.nummer = nummer;
			raeume = new Raum[anzahlRaeume];
			
			for (int i = 0; i < raeume.length; i++) {
				raeume[i] = new Raum(i);
			}
		}
		public int getAnzahlRaeume() {
			return raeume.length;
		}
		
		@Override
		public String toString() {
			return String.format("Stockwerk %d. Anzahl R�ume: %d" ,nummer, getAnzahlRaeume()) ;
		}	
	} // End of Stockwerk
	

	private Stockwerk[] stockwerke;
	private String strasse;
	private String nummer; 
	public Gebaeude(String strasse, String nummer, int anzahlStockwerke, int anzahlRaeumeproStockwerk) {
		this.nummer = nummer;
		this.strasse = strasse;
		
		if(anzahlStockwerke < 1) {
			throw new IllegalArgumentException("Anzahl der Stockwerke muss mindestens 1 sein");
		}
		stockwerke = new Stockwerk[anzahlStockwerke];
		
		for( int i = 0 ; i < stockwerke.length ; i++) {
			stockwerke[i] = new Stockwerk(i , anzahlRaeumeproStockwerk);
		}
		
	}
	
	public Stockwerk getStockwerk(int stockerkNummer) {
		return stockwerke[stockerkNummer];
	}
	
	public String toString() {
		return String.format("Geb�ude an %s %s (Stockwerke: %d, R�ume pro Stockwerk: %d)" ,	strasse,
																							nummer,
																							getAnzahlStockwerke(),
																							stockwerke[0].getAnzahlRaeume()) ;
	}

	public int getAnzahlStockwerke() {
		return stockwerke.length;
	}
	
	public Stockwerk.Raum getRaum(int stockwerkNummer , int raumNummer) {
		
		Stockwerk sw = getStockwerk(stockwerkNummer);
		
		return sw.raeume[raumNummer];
	}
}
