package aufgabe_gebaeude;

import aufgabe_gebaeude.Gebaeude.Stockwerk;

public class AufgabeNestedgebaeude {
	
	public static void main(String[] args) {
		
		int anzahlStockwerke = 3;
		int anzahlRaeumeproStockwerk = 10 ;
		
		Gebaeude g1 = new Gebaeude("Hauptstr", "45", anzahlStockwerke, anzahlRaeumeproStockwerk);
		
		System.out.println(g1);
		
		Stockwerk sw1 = g1.getStockwerk(0);
		System.out.println(sw1);
		
		Object r =g1.getRaum(0, 2);
		System.out.println(r); //Raum 0.2 / Hauptstr. 45
		
	}

}
