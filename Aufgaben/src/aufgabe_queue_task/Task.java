package aufgabe_queue_task;

public class Task {
	
	public enum Priority{
		LOW, NORMAL , HIGH
	}
	
	private final String description;
	private final Priority priority;
	
	public Task(String description, Priority priority) {
		this.description = description;
		this.priority = priority;
	}
	
	public Priority getPriority() {
		return priority;
	}
	
	@Override
	public String toString() {
		return description + " " + priority;
	}
	
	public String getDescription() {
		return description;
	}
	
}
