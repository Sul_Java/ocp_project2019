package aufgabe_queue_task;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import aufgabe_queue_task.Task.Priority;

public class AufgabeQueueTask {

	public static void main(String[] args) {
		
		Task t1 = new Task("Autowäsche", Priority.LOW);
		Task t2 = new Task("Einkaufen", Priority.NORMAL);
		Task t3 = new Task("Rechnung bezahlen", Priority.HIGH);
		
		Comparator<Task> cmp = (task1 , task2) -> {
			int erg = task2.getPriority().compareTo(task1.getPriority());
			
			if (erg == 0 ) {
				erg = task1.getDescription().compareTo(task2.getDescription());
			}
			
			return erg;
		};
		
		Queue<Task> queue = new PriorityQueue<>(cmp);
		queue.add(t1);
		queue.offer(t2);
		queue.offer(t3);
		queue.offer(new Task("Java Lernen", Task.Priority.NORMAL));
		
		while (!queue.isEmpty()) {
			System.out.println(queue.poll());
			
		}
		
	}
}
