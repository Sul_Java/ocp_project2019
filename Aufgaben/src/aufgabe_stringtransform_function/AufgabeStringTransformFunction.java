package aufgabe_stringtransform_function;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

class StringTransform{
	
	private List<UnaryOperator<String>> transformations = new ArrayList<>();
	
	StringTransform addTransformation(UnaryOperator<String> transformation) {
		transformations.add(transformation);
		
		return this;
	}
	
	String process(String s) {
		for (UnaryOperator<String> t : transformations) {
			s = t.apply(s);
		}
		return s;
	}
}

public class AufgabeStringTransformFunction {
	
	public static void main(String[] args) {

	    // Transformationen vordefinieren:
	    StringTransform t1 = new StringTransform()
	            .addTransformation( s -> s.toUpperCase() )
	            .addTransformation( s -> s + "!" );

	    // Transformationen durchführen:
	    String s = t1.process("Hallo");
	    System.out.println(s); // HALLO!

	    s = t1.process("Java ist toll");
	    System.out.println(s); // JAVA IST TOLL!
	    
	   
	}

}
