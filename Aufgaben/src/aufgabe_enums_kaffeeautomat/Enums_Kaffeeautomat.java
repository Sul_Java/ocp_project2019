package aufgabe_enums_kaffeeautomat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

enum KA{
	KAFFEE (1),
	CAPPUCCINO (1.20),
	ESPRESSO (1.13);
	
	private double prise;
	KA(double prise){
		this.prise = prise;
	}
	
	public double getPrise() {
		return prise;
	}
	
	@Override
	public String toString() {
		return name() +" kostet = " + prise;
	}
}

public class Enums_Kaffeeautomat {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println(getpreisliste());
		
		sortPreisList( (o1,o2) -> o2.compareTo(o1));
		
	}
	
	static List<KA> getpreisliste() {
		List<KA> preisList = new ArrayList<>();
		preisList.add(KA.KAFFEE);
		preisList.add(KA.CAPPUCCINO);
		preisList.add(KA.ESPRESSO);
		
		return preisList;
	}
	
	static void sortPreisList(Comparator<KA> comp) {
		
		List<KA> preisList = getpreisliste();
		Collections.sort(preisList , comp);
		System.out.println(preisList);
		
	}

}
