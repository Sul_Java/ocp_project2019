package io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class Methoden_mark_reset_skip {
	
	public static void main(String[] args) throws IOException{
//		testSkip();
		testMarkAndReset();
	}
	
	static void testMarkAndReset() throws IOException{
		
		try (Reader in = new StringReader("aXbYcZd")){
			
			System.out.println( (char)in.read() );
			
			in.mark(10);
			System.out.println( (char)in.read() );
			System.out.println( (char)in.read() );
			
			in.read();
			in.read();
			
			in.reset();
			System.out.println( (char)in.read() );
			System.out.println( (char)in.read() );
		} 
		
	}
	
	static void testSkip() throws IOException{
		
		try (Reader in = new StringReader("aXbYcZd")){
			
			System.out.println( (char)in.read() );
			System.out.println( (char)in.read() );
			
			in.skip(3);
			System.out.println( (char)in.read() );
			
		} 
		
	}

}
