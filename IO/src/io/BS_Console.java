package io;

import java.io.Console;
import java.io.FileNotFoundException;
import java.time.LocalDate;

public class BS_Console {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Console c = System.console();
		
		c.format("heute ist %s %n", LocalDate.now());
		
	}

}
