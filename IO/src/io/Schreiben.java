package io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Schreiben {
	
	public static void main(String[] args) throws IOException {
		Writer out = null;
		try {
			out = new FileWriter("def.txt");
			
			out.write('d');
			out.write('b');
			out.write('c');
			
		} catch (IOException e) {
			System.out.println("Fehler beim �ffnen oder Schreiben ");
		}finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				System.out.println("Fehler beim Schlissen der Datei");
			}
		}
		
	}

}
