package serial;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class B01 {
	
//	@SuppressWarnings("serial")
	static class PKW implements Serializable{
		String hersteller;
		int baujahr;
		public PKW(String hersteller, int baujahr) {
			this.hersteller = hersteller;
			this.baujahr = baujahr;
		}
		
		@Override
		public String toString() {
			return "PKW [hersteller = " + hersteller + ", baujahr = " + baujahr + "]";
		}
	}
	
	public static void main(String[] args) {
		
		String file = "pkw01";
		
		try( ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
			
			PKW pkw1 = new PKW("Audi" , 2001);
			
			oos.writeObject(pkw1);
			System.out.println(pkw1);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
			
			PKW obj = (PKW)ois.readObject();
			System.out.println(obj);
			
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
