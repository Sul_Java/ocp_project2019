package files;

import java.io.File;
import java.io.FileFilter;

public class Klasse_File_list_Methoden {
	
	public static void main(String[] args) {
		
		File[] roots = File.listRoots();
		
		for (File file : roots) {
			System.out.println(file);
		}
		
		File dir = new File("C:\\");
		String[] arr = dir.list();
		for (String string : arr) {
			System.out.println(string);
		}
		
		System.out.println("_______________________");
		
		File dir2 = new File("C:\\Windows");
		
		FileFilter filter = null;
		File[] items = dir2.listFiles(File::isDirectory);
		
		for (File file : items) {
			System.out.println(file);
		}
		
	}

}
