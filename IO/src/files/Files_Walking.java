package files;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Files_Walking {
	
	public static void main(String[] args) {
		
		Path dir = Paths.get(".");
		
		walkMit_newDirectoryStream(dir);
		mit_walk_v1(dir);
		mit_walk_v2(dir);
		
	}
	
	private static void mit_walk_v2(Path dir) {
		System.out.println("******************");
		
		try {
			int maxDepth = 10;
			Files.walk(dir , maxDepth ).forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private static void mit_walk_v1(Path dir) {
		System.out.println("******************");
		
		try {
			Files.walk(dir).forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void walkMit_newDirectoryStream(Path dir) {
		
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(dir )){
			for (Path path : ds) {
				System.out.println(path);
				if (Files.isDirectory(path)) {
					walkMit_newDirectoryStream(path);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
