package files;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFileAttributes;
import java.util.concurrent.TimeUnit;

public class Files_Attribute {
	
	static Path testSource;
	
	static {
		Path src = Paths.get("abc.txt").toAbsolutePath(); 
		try {
			testSource = Files.copy(src, src.getParent().resolve("buchstaben"), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	public static void main(String[] args) throws IOException {
		
//		simplemethod();
//		time();
		attributes();
		
	}
	
	static void attributes() throws IOException {
		
		BasicFileAttributes basicAtts = Files.readAttributes(testSource, BasicFileAttributes.class);
		System.out.println(basicAtts.size());
		System.out.println(basicAtts.lastAccessTime());
		
		DosFileAttributes dosAtts = null;
		PosixFileAttributes posixAtts = null;
		
		Object sizeObj = Files.getAttribute(testSource, "size");
		System.out.println(Files.getAttribute(testSource,"dos:hidden"));
		
		Files.setAttribute(testSource, "dos:hidden", true);
		System.out.println(Files.getAttribute(testSource,"dos:hidden"));
		
	}

	static void time() throws IOException {
		
		FileTime time  = Files.getLastModifiedTime(testSource);
		System.out.println(time);
		
		time = FileTime.from(0 , TimeUnit.DAYS);
		Files.setLastModifiedTime(testSource, time);
		time = Files.getLastModifiedTime(testSource);
		
		System.out.println(time);
		
	}
	
	static void simplemethod() throws IOException {
		
		boolean result = Files.isRegularFile(testSource);
		System.out.println(result);
		
		long size = Files.size(testSource);
		System.out.println(size);
		
		result = Files.isExecutable(testSource);
		System.out.println(result);
		
		result = Files.isHidden(testSource);
		System.out.println(result);
		
		result = Files.isReadable(testSource);
		System.out.println(result);
		
		result = Files.isWritable(testSource);
		System.out.println(result);
		
		System.out.println("********************");
		
	}

}
