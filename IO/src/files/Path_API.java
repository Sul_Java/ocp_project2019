package files;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Path_API {
	
	public static void main(String[] args) {
		
//		testEquals();
//		test();
		
		Path p1 = Paths.get("C:\\Windows");
		Path p2 = Paths.get("C:\\Java\\bin");
		
		System.out.println(p1);
		System.out.println(p2);
		
		Path p3 = p1.resolve(p2);
		System.out.println(p3);
		
	}

	static void test() {
		
		Path path = Paths.get("a/b");
		
		System.out.println(path.getParent());
	}
	
	static void testEquals() {
		
		Path p1 = Paths.get("a\\..\\a\\b\\c");
		
		Path p2 = Paths.get("a\\b\\c");
		
		System.out.println(p1 == p2); //false
		System.out.println(p1.equals(p2)); //true
		
	}
}
