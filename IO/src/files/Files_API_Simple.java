package files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Files_API_Simple {
	
	public static void main(String[] args) {
		
		testExists();
		testIsSameFile();
		testInfoMethoden();
	}
	
	
	
	static void testInfoMethoden() {
		System.out.println("******** test");
		
		Path path = Paths.get("abc.txt");
		
		long size;
		try {
			size = Files.size(path);
			System.out.println(size);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(Files.isDirectory(path,LinkOption.NOFOLLOW_LINKS));
		System.out.println(Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS));
		System.out.println(Files.isWritable(path));
		
	}
	
	static void testIsSameFile() {
		System.out.println(" ****** isSameFile");
		
		Path p1 = Paths.get("src");
		Path p2 = Paths.get("../IO/src");
		
		System.out.println(p1.equals(p2));
		
		try {
			boolean result = Files.isSameFile(p1, p2);
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	static void testExists() {
		
		Path p1 = Paths.get("nichtda");
		
		LinkOption[] options = {LinkOption.NOFOLLOW_LINKS};
		boolean result = Files.exists(p1, options );
		
		result = Files.exists(p1, LinkOption.NOFOLLOW_LINKS);
		result = Files.exists(p1);
		
		System.out.println(p1);
		System.out.println(result);
		
		p1 = Paths.get("src");
		
		result = Files.exists(p1, LinkOption.NOFOLLOW_LINKS);
		result = Files.exists(p1);
		
		System.out.println(p1);
		System.out.println(result);
		
	}

}
