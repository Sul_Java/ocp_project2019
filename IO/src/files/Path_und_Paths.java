package files;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Path_und_Paths {
	
	public static void main(String[] args) {
		
		Path p1 = Paths.get("C:\\CCLauncher-Client-3\\jre\\bin");
		System.out.println(p1);
		
		Path p2 = FileSystems.getDefault().getPath("C:\\CCLauncher-Client-3\\jre\\bin");
		System.out.println(p2);
		
		System.out.println(p1.equals(p2));
		
	}

}
