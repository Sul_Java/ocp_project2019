package aufgaben.arrays;

import java.util.Arrays;
import java.util.Random;

public class KleineAufgabeIO {
	
	public static void main(String[] args)  {
		
		int len = new Random().nextInt(20) + 1 ;
		int min = 0 , max = 16;
		
		int[] arr1 = ArrayUtils.createIntArray(len, min, max);
		System.out.println(Arrays.toString(arr1));
		
		ArrayUtils.saveToFile2(arr1 , "array.txt");
		
		int[] arr2 = ArrayUtils.loadFromFile2("array.txt");
		System.out.println(Arrays.toString(arr2));
		
		System.out.println("Referenzenvergleich: " + (arr1 == arr2)); //false
		System.out.println("Inhaltenvergleichen: " + Arrays.equals(arr1, arr2)); //true
		
	}

}
