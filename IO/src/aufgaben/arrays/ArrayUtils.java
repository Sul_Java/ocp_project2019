package aufgaben.arrays;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

public class ArrayUtils {

	public static int[] createIntArray(int len, int min, int max) {
		int[] arr = new int[len];
		Random random = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(max - min+1)-min;
		}
		return arr;
	}

	public static void saveToFile(int[] arr, String file)  {
		
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))){
			out.writeObject(arr);
		}catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		
	}

	public static int[] loadFromFile(String file) {
		
		try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))){
			return (int[])in.readObject();
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			} catch (ClassNotFoundException e) {
				throw new IllegalArgumentException(e);
			}
	}
	
	public static void saveToFile2(int[] arr, String file)  {
		
		try(PrintWriter out = new PrintWriter(file)){
			
			for (int i : arr) {
				out.println(i);
			}
			
		}catch (FileNotFoundException e) {
			throw new UncheckedIOException(e);
		}
		
	}
	
	public static int[] loadFromFile2(String file) {
		
		try {
			return Files.lines(Paths.get(file))
					.mapToInt(Integer::parseInt)
					.toArray();
			
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		
	}

}
