package aufgaben.file;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FlatFilesCounterV1 implements FilesCounter {
	
	private final Path dir;

	public FlatFilesCounterV1(String dirName) {
		dir = Paths.get(dirName);
	}
	
	private static boolean hasExtension(Path file , String extension) {
		String fileName = file.getFileName().toString();
		
		int posOfPoint = fileName.lastIndexOf('.');
		if (posOfPoint <= 0 || posOfPoint == fileName.length()-1) {
			return false;
		}
		return fileName.substring(posOfPoint).equals(extension);
	}

	@Override
	public int count(String extension)  throws java.io.IOException{
		
		if (Files.isDirectory(dir)) {
			throw new NotDirectoryException(dir.toString());
		}
		
		DirectoryStream.Filter<Path> filter = path -> {
			if (Files.isRegularFile(path)) {
				return false;
			}
			
			return hasExtension(path , extension);
		};
		
		int count = 0;
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(dir ,filter)){
			for (@SuppressWarnings("unused") Path path : ds) {
				count++;
			}
		}
		return count;
	}

}
