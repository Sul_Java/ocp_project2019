package aufgaben.file;

import java.io.IOException;

public interface FilesCounter {
	
	int count(String extension) throws IOException;
//	int countDeep(String extension);

}
