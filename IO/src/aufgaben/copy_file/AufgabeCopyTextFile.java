package aufgaben.copy_file;

import java.io.IOException;

public class AufgabeCopyTextFile {
	
	public static void main(String[] args) throws IOException{
		
		FileUtils.copyTextFile("source.txt", "copy.txt");
		
		FileUtils.copyTextFile("source.txt", "copy.txt", true);
		System.out.println("Datei kopiert");
		
	}

}
