package aufgaben;

import java.io.File;
import java.util.Arrays;

public class AufgabeKlasseFileDirsCount {
	
	public static void main(String[] args) {
		
		File dir = new File("C:\\");
		
		File[] files = dir.listFiles(File::isDirectory);
		int count = 0;
		for (File file : files) {
			count++;
		}
		
		System.out.println(count);
		
		System.out.println("_________________________");
		
		Long count2 = Arrays.stream(files).filter(File::isDirectory).count();
		System.out.println(count2);
		
		System.out.println("_________________________");
		
		File dir3 = new File("C:\\");
		
		int c2 = getCountDirs(dir3);
		System.out.println(c2);
	}
	static int getCountDirs(File dir) {
		File[] files = dir.listFiles(File::isDirectory);
		
		int count = files.length;
		
		for (File file : files) {
			count += getCountDirs(file);
		}
		
		return count;
	}
}
