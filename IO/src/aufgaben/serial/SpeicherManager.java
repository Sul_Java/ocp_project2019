package aufgaben.serial;

import java.io.Serializable;

public class SpeicherManager extends Dienst implements Serializable {
	int size;
	transient Defragmentierung defrag;
	
	
	
	public SpeicherManager(int size, Defragmentierung defrag) {
		super();
		this.size = size;
		this.defrag = defrag;
	}



	public String toString() {
	    return "Manager. Size: " + size+ ". Defrag-Dienst: " + defrag;
	}
}
