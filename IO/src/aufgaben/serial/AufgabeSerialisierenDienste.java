package aufgaben.serial;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class AufgabeSerialisierenDienste {
	
	public static void main(String[] args) {
		
		SpeicherManager sManager = new SpeicherManager(2000, new Defragmentierung (3000, "C:\\"));
		System.out.println(sManager);
		
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("sManager.bin"))){
			oos.writeObject(sManager);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("SManager.bin"))){
			ois.readInt();
		}catch (IOException  e) {
			e.printStackTrace();
		}
		
	}

}
