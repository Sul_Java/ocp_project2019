package cc.quiz;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;


// todo: dulicates in answers/topics ?
public class QuestionBuilderTest {
	
	private final Answer a1 = new Answer("a1");
	private final Answer a2 = new Answer("a2");
	
	@Test
	void duplicateTopicsAreIgnored() {
		Question q = new Question.Builder()
				.text("Fragestellung")
				.topic("Basics")
				.topic("Basics")
				.answer( a1 )
				.answer( a2 )
				.build();
		
		assertEquals(1,  q.getTopics().size());
	}
	@Test
	void answerFailsOnDuplicateAnswer() {
		
		assertThrows(IllegalArgumentException.class, () ->
			new Question.Builder()
					.text("Fragestellung")
					.topic("Basics")
					.answer( a1 )
					.answer( new Answer(a1.getText(), a1.isCorrect()) )
		);
				
	}

	@Test
	void buildCanCreateQuestion() {
		Question q = new Question.Builder()
							.text("Fragestellung")
							.topic("Basics")
							.answer( a1 )
							.answer( a2 )
							.build();
		
		assertNotNull(q);
	}
	
	@Test
	void testBuildWithComment() {
		String expected = "my comment";
		
		Question instance = new Question.Builder()
									.text("Frage")
									.topic("t1")
									.answer(a1)
									.answer(a2)
									.comment(expected)
									.build();

		assertEquals( expected, instance.getComment() );
	}
	
	@Test
	void commentFailsWithNull() {
		
		assertThrows(IllegalArgumentException.class, () ->
		
			new Question.Builder()
					.text("Frage")
					.topic("t1")
					.answer(a1)
					.answer(a2)
					.comment(null) )
		;
				
	}
	

	@Test
	void buildFailsWithAnswersLessThenTwo() {

		assertThrows(IllegalStateException.class, () -> {
			new Question.Builder()
			.text("Frage")
			.topic("Basics")
			.build();
		} );
	}
	
	@Test
	void buildFailsWithEmptyTopicList() {
		
		assertThrows(IllegalStateException.class, () -> {
			new Question.Builder()
			.text("Frage")
			.answer(a1)
			.answer(a2)
			.build();
		} );
	}
	
	@Test
	void buildFailsWithoutQuestionText() {
		
		assertThrows(IllegalStateException.class, () -> {
			new Question.Builder()
			.topic("Basics")
			.answer(a1)
			.answer(a2)
			.build();
		} );
	}
	
	@Test
	void buildFailsIfAnyAnswerIsNull() {
		
		assertThrows(IllegalStateException.class, () -> {
			new Question.Builder()
			.text("Frage")
			.topic("Basics")
			.answer(a1)
			.answer(null)
			.build();
		} );
	}
	
	@Test
	void buildFailsIfAnyTopicIsNull() {
		
		assertThrows(IllegalStateException.class, () -> {
			new Question.Builder()
			.text("Frage")
			.topic(null)
			.answer(a1)
			.answer(a2)
			.build();
		} );
	}
	
	
	
}





